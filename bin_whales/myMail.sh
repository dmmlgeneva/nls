#!/bin/bash
#
#SBATCH --nodelist=omura
#SBATCH --output=SO/mail.txt
#SBATCH --error=SE/mail.txt
#SBATCH --job-name=MailOK

fList=$(find SE/"$1"* -size +0)

printf "Hotovka (launched after $3 ) \n List of error files:\n%s \n" "$fList" | mail -s "whales $1 $2" magda_gregorova@yahoo.com

