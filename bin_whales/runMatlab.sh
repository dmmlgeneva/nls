#!/bin/bash
# run matlab code on whales

cd ..
exp=ShuffHouse;
algo=krls;
echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',4); testModels('${exp}','${algo}','gauss',4)" | matlab -nodesktop -nosplash
##algo=eNet\(0.5\);
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',4); testModels('${exp}','${algo}','gauss',4)" | matlab -nodesktop -nosplash
#algo=eNet\(0.8\);
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',4); testModels('${exp}','${algo}','gauss',4)" | matlab -nodesktop -nosplash
#algo=eNet\(0.2\);
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',4); testModels('${exp}','${algo}','gauss',4)" | matlab -nodesktop -nosplash
#algo=eNet\(0.5\)Resc;
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',4); testModels('${exp}','${algo}','gauss',4)" | matlab -nodesktop -nosplash
#algo=eNet\(0.8\)Resc;
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',4); testModels('${exp}','${algo}','gauss',4)" | matlab -nodesktop -nosplash
#algo=eNet\(0.2\)Resc;
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',4); testModels('${exp}','${algo}','gauss',4)" | matlab -nodesktop -nosplash
#algo=L1\(0.1\);
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',4); testModels('${exp}','${algo}','gauss',4)" | matlab -nodesktop -nosplash
#algo=GL\(0.1\);
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',4); testModels('${exp}','${algo}','gauss',4)" | matlab -nodesktop -nosplash
#algo=L1\(0\);
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',4); testModels('${exp}','${algo}','gauss',4)" | matlab -nodesktop -nosplash
#algo=GL\(0\);
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',4); testModels('${exp}','${algo}','gauss',4)" | matlab -nodesktop -nosplash
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','lin',1); testModels('${exp}','${algo}','lin',1)" | matlab -nodesktop -nosplash
echo "addpath(genpath('./functions')); compareModels('${exp}','gauss4')" | matlab -nodesktop -nosplash

