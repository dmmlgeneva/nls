function cp_mseLin()
% CP_MSELIN - compare mse across test for the lineear models
%
%
% CREATED: MG - 03/06/2017

%% nicer colors
colorOrder=[    0    0.4470    0.7410; % modra
    0.9290    0.6940    0.1250 % zluta
  0.4660    0.6740    0.1880; % zelena
    0.4940    0.1840    0.5560; %fialova
    0.8500    0.3250    0.0980; % cervena
    0.3010    0.7450    0.9330; % bledemodra
0.6350    0.0780    0.1840; % tmave cervena
    ];
set(groot,'defaultAxesColorOrder',colorOrder)


%% define the experiments - select 1 here
expCode = 'exp001'; krn = 'polyHom2'; eName = 'E1';  lPosition = [0.58 0.4 0.32 0.28];
%expCode = 'exp004'; krn = 'polyHom3'; eName = 'E2';  lPosition = [0.58 0.35 0.32 0.28];
%expCode = 'exp005'; krn = 'polyHom3'; eName = 'E3';   lPosition = [0.58 0.15 0.32 0.28];

dataType = 'testData';
tLVec = 50:20:150;

%% define the algos
algo{1} = 'LinL1';
algo{2} = 'LinGL';
algo{3} = 'LinENet(0.9)';
algo{4} = 'krls';

%% get the method mse
%%%%%%%%%%%%%%%%%%%%%

f1 = figure;
set(f1,'PaperUnits','centimeters')
set(f1,'PaperPosition',[0 0 5 4])

for mIdx = 1:numel(algo)
  krnM = krn;
  % load test sparsity
  if mIdx<4
    krnM = 'lin1';
  end
  % load test mse
  load(['../nls_data/expResults/',expCode,'/testSummary_',algo{mIdx},'_',krnM,'.mat'],'mseTest')
  % get avg mseTest
  avgMseTest =  mean(reshape(cell2mat(struct2cell(mseTest)),[],size(mseTest,2)),2);
  % order by train size
  fS = fields(mseTest);
  [~,fIdx] = sort(cellfun(@(x) str2num(x(4:end)),fS));
  avgMseTest = avgMseTest(fIdx);

  % plot mse
  plot(avgMseTest,'LineWidth',2,'Color',colorOrder(mIdx,:));
  hold on

end
set(gca,'XTick',[1:6]','FontSize',5); set(gca,'XtickLabel',[50:20:150]','FontSize',5);
%set(gca, 'YScale', 'log');
yl = ylabel('MSE','FontSize',6);
yP = get(yl); set(yl,'Position',yP.Position - [0.3 0 0]);
xl = xlabel('Train size','FontSize',6);
xP = get(xl); set(xl,'Position',xP.Position - [0 0.01 0]);
%ll = legend({'L','GL','eNet','Krls'},'Interpreter','latex');
%set(ll,'FontSize',6.5);
%set(ll,'Position',lPosition)
tt = title([eName,': MSE over test']);
set(tt,'FontSize',7);
%set(f1,'PaperSize',[3 2])
print(f1,['/home/magda/Dropbox/School/MyTexts/xNonlinearSparsity/ICDM2017/Pics/cpMseLin_',expCode,krn],'-dpng');
close(f1);





end



