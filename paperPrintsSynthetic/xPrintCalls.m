%%% MG 3/6/2017
%% print model comparisons
clear
expCode = 'exp005'
a1.algo = 'krls'
a1.kernelType = 'polyHom';
a1.kernelParam = 3;
a2.algo = 'L1'
a2.kernelType = a1.kernelType;
a2.kernelParam = a1.kernelParam;
compareModels(expCode,a1,a2)


compModels(expCode,['L1-d_',a1.kernelType,num2str(a1.kernelParam)'],a1,a2)

compareModelsSP(expCode,a1)

load(['../nls_data/expData/exp001/testData.mat'])
var(y)
plot(y)
load(['../nls_data/expData/exp004/testData.mat'])
var(y)
load(['../nls_data/expData/exp005/testData.mat'])
var(y)