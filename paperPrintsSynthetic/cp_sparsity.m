function cp_sparsity()
% CP_SPARSITY - compare derivatives across test
%
%
% EXAMPLE: cp_derivative()
%
% CREATED: MG - 04/06/2017

%% nicer colors
colorOrder=[0.4660    0.6740    0.1880; % zelena
    0.8500    0.3250    0.0980; % cervena
    0.3010    0.7450    0.9330; % bledemodra
    0.4940    0.1840    0.5560; %fialova
    0.6350    0.0780    0.1840; % tmave cervena
    0    0.4470    0.7410; % modra
    0.9290    0.6940    0.1250 % zluta
    ];
set(groot,'defaultAxesColorOrder',colorOrder)

%% define the experiments - select 1 here
%expCode = 'exp001'; krn = 'polyHom2'; eName = 'E1'; nonIdx = logical([zeros(4,1); ones(16,1)]); lPosition = [0.58 0.55 0.32 0.28]; makelog = 1;
%expCode = 'exp004'; krn = 'polyHom3'; eName = 'E2'; nonIdx = logical([zeros(6,1); ones(6,1)]); lPosition = [0.58 0.64 0.32 0.28]; makelog = 1;
expCode = 'exp005'; krn = 'polyHom3'; eName = 'E3'; nonIdx = logical([zeros(6,1); ones(6,1)]); lPosition = [0.13 0.12 0.32 0.28]; makelog = 1;

%expCode = 'exp001'; krn = 'poly4'; eName = 'E1'; nonIdx = logical([zeros(4,1); ones(16,1)]); lPosition = [0.58 0.55 0.32 0.28]; makelog = 0;
%expCode = 'exp005'; krn = 'gauss1'; eName = 'E3'; nonIdx = logical([zeros(6,1); ones(6,1)]); lPosition = [0.13 0.12 0.32 0.28]; makelog = 0;

dataType = 'testData';
tLVec = 50:20:150;

%% define the algos
%algo{1} = 'denovas1(0)';
%algo{2} = 'denovas1(0.1)'
algo{1} = 'L1';
algo{2} = 'GL';
algo{3} = 'eNet(0.7)';
algo{4} = 'krls';

%% get the method mse
%%%%%%%%%%%%%%%%%%%%%

f1 = figure;
set(f1,'PaperUnits','centimeters')
set(f1,'PaperPosition',[0 0 5 4])

for mIdx = 1:numel(algo)
  % load test sparsity
  load(['../nls_data/expResults/',expCode,'/testSummary_',algo{mIdx},'_',krn,'.mat'],'sparsityTest')
  % order by train size
  fS = fields(sparsityTest);
  [~,fIdx] = sort(cellfun(@(x) str2num(x(4:end)),fS));
  % get avg sparsityTest
  sparsityTest = reshape(mean(cell2mat(squeeze(struct2cell(sparsityTest))),2),[],size(fS,1));
  sparsityTest = sparsityTest(:,fIdx); % reorder according to trainLength
  sparsityTest = bsxfun(@rdivide, sparsityTest, sum(sparsityTest)); % and normalize to sum to 1 across all derivatives
  nonActive = sparsityTest(nonIdx,:);
  avgNActNorm = sum(abs(nonActive));

  % plot mse
  plot(avgNActNorm,'LineWidth',2,'Color',colorOrder(mIdx,:));
  hold on

end
set(gca,'XTick',[1:6]','FontSize',5); set(gca,'XtickLabel',[50:20:150]','FontSize',5);
if makelog
  set(gca, 'YScale', 'log');
end
yl = ylabel('Sparsity','FontSize',6);
yP = get(yl); set(yl,'Position',yP.Position - [0.3 0 0]);
xl = xlabel('Train size','FontSize',6);
xP = get(xl); set(xl,'Position',xP.Position - [0 0.0 0]);
% ll = legend({'$\mathcal{R}^L$','$\mathcal{R}^{GL}$','$\mathcal{R}^{EN}$','Krls'},'Interpreter','latex');
% set(ll,'FontSize',6.5);
% set(ll,'Position',lPosition)
tt = title([eName,': Sparsity over test']);
set(tt,'FontSize',7);
%set(f1,'PaperSize',[3 2])
print(f1,['/home/magda/Dropbox/School/MyTexts/xNonlinearSparsity/ICDM2017/Pics/cpSparsity_',expCode,krn],'-dpng');
close(f1);





end



