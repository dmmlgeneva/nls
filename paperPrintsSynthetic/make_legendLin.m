function make_legendLin()
% CP_MSE - compare derivatives across test
%
%
% CREATED: MG - 03/06/2017

%% nicer colors
colorOrder=[    0    0.4470    0.7410; % modra
    0.9290    0.6940    0.1250 % zluta
  0.4660    0.6740    0.1880; % zelena
    0.4940    0.1840    0.5560; %fialova
    0.8500    0.3250    0.0980; % cervena
    0.3010    0.7450    0.9330; % bledemodra
0.6350    0.0780    0.1840; % tmave cervena
    ];
set(groot,'defaultAxesColorOrder',colorOrder)

f1 = figure;
set(f1,'PaperUnits','centimeters')
set(f1,'PaperPosition',[0 0 10 1])
p1 = plot(rand(5,4),'LineWidth',2);
set(gca,'XColor','w','YColor','w','xtick',[],'ytick',[])
%set(gca,'xcolor','w','ycolor','w','xtick',[],'ytick',[])
set(gca,'box','off')
set(p1, 'visible', 'off');
ll = legend({'L','GL','eNet','Krls'},'Interpreter','latex','Orientation','horizontal');
set(ll,'Color',[1 1 1])
set(ll,'EdgeColor',[1 1 1])
%set(ll,'box','off')
%set(ll,'FontSize',6.5);
%set(ll,'Position',[0.1 0.1 0.9 0.9])
print(f1,['/home/magda/Dropbox/School/MyTexts/xNonlinearSparsity/ICDM2017/Pics/legendLin'],'-dpng');
close(f1);





end



