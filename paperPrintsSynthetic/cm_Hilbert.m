function cm_Hilbert()
% cm_Hilbert - compare MSE nonhilbert and gilber
%
% CREATED: MG - 03/06/2017

%% nicer colors
colorOrder=[0    0.4470    0.7410;
    0.4660    0.6740    0.1880;
    0.8500    0.3250    0.0980;
    0.3010    0.7450    0.9330;
    0.9290    0.6940    0.1250;
    0.4940    0.1840    0.5560;
    0.6350    0.0780    0.1840];
set(groot,'defaultAxesColorOrder',colorOrder)


%% define the experiments
expCode{1} = 'exp001'; krn{1} = 'polyHom2'; nonIdx{1} = logical([zeros(4,1); ones(16,1)]);
expCode{2} = 'exp004'; krn{2} = 'polyHom3'; nonIdx{2} = logical([zeros(6,1); ones(6,1)]);
expCode{3} = 'exp005'; krn{3} = 'polyHom3'; nonIdx{3} = logical([zeros(6,1); ones(6,1)]);

%% define the algos
algo{1} = 'L1H(0.1)';
%algo{2} = 'denovas1(0.1)'
algo{2} = 'L1';

%% plot line type
linT{1} = '--';
linT{2} = '-';

f1 = figure;
set(f1,'PaperUnits','centimeters')
set(f1,'PaperPosition',[0 0 5 4])
pIdx = 0;
for eIdx = 1:3 % loop for experiments
  for mIdx = 1:2 % loop for algos
    
    % load test mse
    load(['../nls_data/expResults/',expCode{eIdx},'/testSummary_',algo{mIdx},'_',krn{eIdx},'.mat'],'mseTest')
    % get avg mseTest
    avgMseTest =  mean(reshape(cell2mat(struct2cell(mseTest)),[],size(mseTest,2)),2);
    % order by train size
    fS = fields(mseTest);
    [~,fIdx] = sort(cellfun(@(x) str2num(x(4:end)),fS));
    avgMseTest = avgMseTest(fIdx);
    
    % plot mse
    pIdx = pIdx+1;
    p{pIdx} = plot(avgMseTest,linT{mIdx},'LineWidth',2,'Color',colorOrder(eIdx,:));
    %plot(bsxfun(@times,ones(15,5),[1,2,3,4,5]),'LineWidth',2)
    hold on
  
end
end
set(gca,'XTick',[1:6]','FontSize',5); set(gca,'XtickLabel',[50:20:150]','FontSize',5);
set(gca, 'YScale', 'log');
yl = ylabel('MSE','FontSize',6);
yP = get(yl); set(yl,'Position',yP.Position - [0.2 0 0]);
xl = xlabel('Train size','FontSize',6);
xP = get(xl); 
%xP = get(xl); set(xl,'Position',xP.Position - [0 0.01 0]);
%ll = legend([p{2} p{4} p{6}],'E1','E2','E3');
%set(ll,'FontSize',5.5);
%set(ll,'Position',[0.14 0.14 0.25 0.15])
% add text to distinguish ours and denovas
a1 = annotation('textarrow','String','$\mathcal{R}^{L}$','FontSize',6.5,'Interpreter','latex')
%set(a1,'Position',[0.33 0.23 0.04 0.04],'HeadLength',3,'HeadWidth',3);
set(a1,'Position',[0.35 0.25 0.04 0.04],'HeadLength',3,'HeadWidth',3);
a2 = annotation('textarrow','String','$\mathcal{R}^{L} + H$','FontSize',6.5,'Interpreter','latex')
%set(a2,'Position',[0.65 0.45 0.04 0.04],'HeadLength',3,'HeadWidth',3);
set(a2,'Position',[0.6 0.4 -0.04 -0.04],'HeadLength',3,'HeadWidth',3);
tt = title('$\mathcal{R}^{L}$ Average MSE test','Interpreter','latex');
set(tt,'FontSize',7);
%set(f1,'PaperSize',[3 2])
print(f1,['/home/magda/Dropbox/School/MyTexts/xNonlinearSparsity/ICDM2017/Pics/cmHilbert',algo{2}],'-dpng');
close(f1);



end





