function csTest_denL1()
% cm_denL1 - compare sparsity Test denovas vs L1 across the three base experiments
%   watchout, this is not clean and neat but messy and quick
%
% CREATED: MG - 03/06/2017

%% nicer colors
colorOrder=[0    0.4470    0.7410;
    0.4660    0.6740    0.1880;
    0.8500    0.3250    0.0980;
    0.3010    0.7450    0.9330;
    0.9290    0.6940    0.1250;
    0.4940    0.1840    0.5560;
    0.6350    0.0780    0.1840];
set(groot,'defaultAxesColorOrder',colorOrder)

%colours = permute(get(gca, 'colororder'), [1 3 2]);
%colours_resize = imresize(get(gca, 'colororder'), 50.0, 'nearest');
%imshow(colours_resize);


%% define the experiments
expCode{1} = 'exp001'; krn{1} = 'polyHom2'; nonIdx{1} = logical([zeros(4,1); ones(16,1)]);
expCode{2} = 'exp004'; krn{2} = 'polyHom3'; nonIdx{2} = logical([zeros(6,1); ones(6,1)]);
expCode{3} = 'exp005'; krn{3} = 'polyHom3'; nonIdx{3} = logical([zeros(6,1); ones(6,1)]);

%% define the algos
algo{1} = 'denovas1(0)';
%algo{2} = 'denovas1(0.1)'
algo{2} = 'L1';

%% plot line type
linT{1} = '--';
linT{2} = '-';

f1 = figure;
set(f1,'PaperUnits','centimeters')
set(f1,'PaperPosition',[0 0 5 4])
pIdx = 0;
for eIdx = 1:3 % loop for experiments
  for mIdx = 1:2 % loop for algos
    
    % load test sparsity
    load(['../nls_data/expResults/',expCode{eIdx},'/testSummary_',algo{mIdx},'_',krn{eIdx},'.mat'],'sparsityTest')
    % order by train size
    fS = fields(sparsityTest);
    [~,fIdx] = sort(cellfun(@(x) str2num(x(4:end)),fS));
    % get avg sparsityTest
    sparsityTest = reshape(mean(cell2mat(squeeze(struct2cell(sparsityTest))),2),[],size(fS,1));
    sparsityTest = sparsityTest(:,fIdx); % reorder according to trainLength
    sparsityTest = bsxfun(@rdivide, sparsityTest, sum(sparsityTest)); % and normalize to sum to 1 across all derivatives
    nonActive = sparsityTest(nonIdx{eIdx},:);
    avgNActNorm = sum(abs(nonActive));
    
    % plot mse
    pIdx = pIdx+1;
    p{pIdx} = plot(avgNActNorm,linT{mIdx},'LineWidth',2,'Color',colorOrder(eIdx,:));
    %plot(bsxfun(@times,ones(15,5),[1,2,3,4,5]),'LineWidth',2)
    hold on
  
end
end
set(gca,'XTick',[1:6]','FontSize',5); set(gca,'XtickLabel',[50:20:150]','FontSize',5);
set(gca, 'YScale', 'log');
yl = ylabel('Sparsity','FontSize',6);
yP = get(yl); set(yl,'Position',yP.Position - [0.3 0 0]);
xl = xlabel('Train size','FontSize',6);
xP = get(xl); set(xl,'Position',xP.Position - [0 0.00 0]);
% ll = legend([p{2} p{4} p{6}],'E1','E2','E3');
% set(ll,'FontSize',5.5);
% set(ll,'Position',[0.14 0.14 0.25 0.15])
% add text to distinguish ours and denovas
a1 = annotation('textarrow','String','$\mathcal{R}^L$','FontSize',6.5,'Interpreter','latex')
set(a1,'Position',[0.7 0.2 -0.04 -0.04],'HeadLength',3,'HeadWidth',3);
a2 = annotation('textarrow','String','DENOVAS','FontSize',6)
set(a2,'Position',[0.7 0.45 -0.04 -0.04],'HeadLength',3,'HeadWidth',3);
tt = title('Sparsity over test sample');
set(tt,'FontSize',7);
%set(f1,'PaperSize',[3 2])
print(f1,'/home/magda/Dropbox/School/MyTexts/xNonlinearSparsity/ICDM2017/Pics/csTest','-dpng');
close(f1);



end





