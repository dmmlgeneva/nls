function cp_derivativeLin()
% CP_DERIVATIVELin - compare derivatives across test
%
%
% EXAMPLE: cp_derivative()
%
% CREATED: MG - 03/06/2017

%% nicer colors
colorOrder=[    0.6350    0.0780    0.1840; % tmave cervena
    0    0.4470    0.7410; % modra
    0.9290    0.6940    0.1250 % zluta
  0.4660    0.6740    0.1880; % zelena
    0.4940    0.1840    0.5560; %fialova
    0.8500    0.3250    0.0980; % cervena
    0.3010    0.7450    0.9330; % bledemodra
    ];
set(groot,'defaultAxesColorOrder',colorOrder)

%% define the experiments - select 1 here
%expCode = 'exp001'; krn = 'polyHom2'; 
%expCode = 'exp004'; krn = 'polyHom3'; 
expCode = 'exp005'; krn = 'polyHom3'; 

dataType = 'testData';
tLVec = 50:20:150;

%% define the algos
%algo{1} = 'denovas1(0)';
%algo{2} = 'denovas1(0.1)'
algo{1} = 'LinL1';
algo{2} = 'LinGL';
algo{3} = 'LinENet(0.9)';
algo{4} = 'krls';

%% get the method sparsity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for mIdx = 1:numel(algo)
  krnM = krn;
  % load test sparsity
  if mIdx<4
    krnM = 'lin1';
  end
  load(['../nls_data/expResults/',expCode,'/testSummary_',algo{mIdx},'_',krnM,'.mat'],'sparsityTest')
  % order by train size
  fS = fields(sparsityTest);
  [~,fIdx] = sort(cellfun(@(x) str2num(x(4:end)),fS));
  % get avg sparsityTest
  sparsity{mIdx} = reshape(mean(cell2mat(squeeze(struct2cell(sparsityTest))),2),[],size(fS,1));
  sparsity{mIdx} = sparsity{mIdx}(:,fIdx); % reorder according to trainLength
  sparsity{mIdx} = abs(sparsity{mIdx});
  sparsity{mIdx} = bsxfun(@rdivide, sparsity{mIdx}, sum(sparsity{mIdx})); % and normalize to sum to 1 across all derivatives
end


%% get the true experiment derivatives
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch expCode
  case 'exp001'
    derTrue = derExp001();
  case 'exp004'
    derTrue = derExp004();
  case 'exp005'
    derTrue = derExp005();
  otherwise
    derTrue = [];
end

f1 = figure;
set(f1,'PaperUnits','centimeters')
set(f1,'PaperPosition',[0 0 9 11])
for tLength=1:6
  spAll = [derTrue' cell2mat(cellfun(@(x) x(:,tLength), sparsity,'UniformOutput',0))];
  s(tLength) = subplot(3,2,tLength);
  bbr = bar(spAll,'edgecolor','none','BarWidth',1);
  set(gca,'FontSize',5)
  xlim([0 size(spAll,1)+1])
  if tLength==1 || tLength==3 || tLength==5
    yl = ylabel('$|| \partial_a f ||_{2_n}$','Interpreter','latex','FontSize',9);
  end
  if tLength>=5
    xl = xlabel('Dimension','FontSize',7);
  end
  tt = title(['Train size = ',num2str(tLVec(tLength))]);
  set(tt,'FontSize',7);
  for mIdx = 1:numel(algo)+1
    set(bbr(mIdx),'facecolor',colorOrder(mIdx,:)); 
  end
end
ll = legend({'True $\quad$ ','Lasso $\quad$','GLasso $\quad$','eNet $\quad$','Krls'},'Orientation','horizontal','Interpreter','latex');
set(ll,'FontSize',7);
set(ll,'Position',[0.15 -0.02 0.8 0.1])
set(ll,'Box','off','FontWeight','bold');
%set(ll,'FontSize',20);

set(s(1),'Position',[0.09    0.73    0.42    0.23])
set(s(2),'Position',[0.57    0.73    0.42    0.23])
set(s(3),'Position',[0.09    0.43    0.42    0.23])
set(s(4),'Position',[0.57    0.43    0.42    0.23])
set(s(5),'Position',[0.09    0.12    0.42    0.23])
set(s(6),'Position',[0.57    0.12    0.42    0.23])

print(f1,['/home/magda/Dropbox/School/MyTexts/xNonlinearSparsity/AISTATS2018/Pics/cpDerivLin_',expCode,krn],'-dpng');
close(f1);

%% functions for true sparsity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function drvNorm = derExp001()
  % actually the same derivative for all the 1st 4 variables
  load(['../nls_data/expData/exp001/',dataType],'y','x')
  n = size(x,1);
  xAct = x(:,1:4);
  drvPointWise = sum(2*xAct,2);
  drvNorm(1:4) = ones(1,4)*sqrt(sum(drvPointWise.^2))/sqrt(n);
  drvNorm(5:20) = zeros(1,16);
  % and normalize to 1
  drvNorm = bsxfun(@rdivide, drvNorm, sum(drvNorm)); % and normalize to sum to 1 across all derivatives
end

function drvNorm = derExp004()
  load(['../nls_data/expData/exp004/',dataType],'y','x','coefs')
  n = size(x,1);
  xAct = x(:,1:6);
  xRpt = kron(xAct,ones(1,6));
  xLng = repmat(xAct,1,6);
  xMtpl = xRpt.*xLng;
  coefs = reshape(coefs,[],6);
  for dIdx = 1:6
    drvPointWise = sum(3*xMtpl.*repmat([coefs(:,dIdx)]',n,1),2);
    drvNorm(dIdx) = sqrt(sum(drvPointWise.^2))/sqrt(n);
  end
  drvNorm(7:12) = zeros(1,6);
  % and normalize to 1
  drvNorm = bsxfun(@rdivide, drvNorm, sum(drvNorm)); % and normalize to sum to 1 across all derivatives
end

function drvNorm = derExp005()
  load(['../nls_data/expData/exp005/',dataType],'y','x')
  n = size(x,1);
  xAct = x(:,1:6);
  xRpt = kron(xAct,ones(1,6));
  xLng = repmat(xAct,1,6);
  xMtpl = xRpt.*xLng;
  % actually the same derivative for all the 1st 6 variables
  drvPointWise = sum(3*xAct,2);
  drvNorm(1:6) = ones(1,6)*sqrt(sum(drvPointWise.^2))/sqrt(n);
  drvNorm(7:12) = zeros(1,6);
  % and normalize to 1
  drvNorm = bsxfun(@rdivide, drvNorm, sum(drvNorm)); % and normalize to sum to 1 across all derivatives
end



end



