function [sel,spTrain,spVal,minPIdx,pGrid] = checkLearning(expCode,algo,kernelType,kernelParam)
%% check validation summary for best MSE
%expCode = 'ShuffHouse';
%algo = 'GL(0.0001)';
%kernelType = 'lin';
%kernelParam = 1;

load(['../nls_data/expResults/',expCode,'/validSummary_',algo,'_',kernelType,num2str(kernelParam),'.mat'])

reps = size(mseValidation,2);
% watchout, the trainLength is not in the correct order
fields(mseValidation);
mseVal = reshape(cell2mat(struct2cell(mseValidation)),[],reps);

sTemp = [squeeze(struct2cell(selected))]';
sel = horzcat(sTemp{:});

sTemp = [squeeze(struct2cell(sparsityTrain))]';
spTrain = horzcat(sTemp{:});

sTemp = [squeeze(struct2cell(sparsityValidation))]';
spVal = horzcat(sTemp{:});

sTemp = [squeeze(struct2cell(minParamIdx))]';
minPIdx = horzcat(sTemp{:});

sTemp = struct2cell(parameterGrid(1));
pGrid = sTemp{:};

end