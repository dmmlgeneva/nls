#!/bin/bash
# run matlab code on baobab

#SBATCH --job-name=runCompile
#SBATCH --output=SO/runCompile
#SBATCH --error=SE/runCompile
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --licenses=matlab@matlablm.unige.ch
#SBATCH --partition=debug
#SBATCH --time=00:15:00


cd ../functions/deploy
echo "compile" | matlab -nodesktop -nosplash
