#!/bin/bash
# run matlab code on baobab

#SBATCH --job-name=runCompile
#SBATCH --output=SO/runCompile
#SBATCH --error=SE/runCompile
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --licenses=matlab@matlablm.unige.ch
#SBATCH --partition=debug
#SBATCH --time=00:15:00

cd ..
exp=exp006;
algo=mean;
echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','lin',1); testModels('${exp}','${algo}','lin',1)" | matlab -nodesktop -nosplash
algo=LinL1;
echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','lin',1); testModels('${exp}','${algo}','lin',1)" | matlab -nodesktop -nosplash
algo=LinGL;
echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','lin',1); testModels('${exp}','${algo}','lin',1)" | matlab -nodesktop -nosplash
algo=L1;
echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','polyHom',2); testModels('${exp}','${algo}','polyHom',2)" | matlab -nodesktop -nosplash
algo=GL;
echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','polyHom',2); testModels('${exp}','${algo}','polyHom',2)" | matlab -nodesktop -nosplash
#algo=denovas1\(10\);
#echo "addpath(genpath('./functions')); sumValidation('${exp}','${algo}','gauss',100); testModels('${exp}','${algo}','gauss',100)" | matlab -nodesktop -nosplash


#algo=L1;
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=L1H\(0.1\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=L1H\(1\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=L1H\(10\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=GL;
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=GLH\(0.1\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=GLH\(1\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=GLH\(10\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=denovas1\(0\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=denovas1\(0.1\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=denovas1\(1\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=denovas1\(10\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=denovas1\(100\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=LinL1;
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','lin',1); testModels('exp005','${algo}','lin',1)" | matlab -nodesktop -nosplash
#algo=LinGL;
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','lin',1); testModels('exp005','${algo}','lin',1)" | matlab -nodesktop -nosplash
#algo=LinENet\(0.9\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','lin',1); testModels('exp005','${algo}','lin',1)" | matlab -nodesktop -nosplash

#algo=eNet\(0.1\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=eNet\(0.1\)Resc;
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=eNet\(0.333\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=eNet\(0.333\)Resc;
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=eNet\(0.5\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=eNet\(0.5\)Resc;
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=eNet\(0.7\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=eNet\(0.7\)Resc;
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=eNet\(0.9\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=eNet\(0.9\)Resc;
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=eNet\(0.95\);
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash
#algo=eNet\(0.95\)Resc;
#echo "addpath(genpath('./functions')); sumValidation('exp005','${algo}','gauss',1); testModels('exp005','${algo}','gauss',1)" | matlab -nodesktop -nosplash





