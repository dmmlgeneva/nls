function sumValidation(expCode,algo,kernelType,kernelParam)
% SUMVALIDATION - summarise the validation results
%
% INPUTS
%   expCode - code of the experiment
%   kernelType - 'poly','polyHom' or 'Gauss'
%   kernelParam - degree of polynomials or sigma for Gauss
%   algo - code of the algo to use; for algos with mu include mu into the algo name in brackets after, e.g. denovas1(0)
%
% EXAMPLE: sumValidation('exp001','L1','poly',2)
%     for deonvas put mu into the algo name: sumValidation('exp001','denovas1(0)','poly',2)
%
% CREATED: MG - 15/05/2017

%% gather validataion results from all replications and trainLengths
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get list of files
fList = dir(['../nls_data/expResults/',expCode,'/valid_',algo,'*']); fList = {fList.name}'; 
fNum = length(fList);
if ~(length(fList)>0)
  error('No cv results available for given input combinations')
end


% from each file extract the validation info
trainLengthVec = [];
for fIdx=1:fNum
  fName = fList{fIdx};
  % keep going only if results for the correct kernel
  if isempty(strfind(fName,[kernelType,num2str(kernelParam)]))
    continue
  end
  % get separators
  pos_ = strfind(fName,'_');
  if ~isempty(strfind(fName,'eNet')) % eNet has an extra underscore in the brackets so drop that
    pos_(1) = [];
  end
  trainLength = str2double(fName(pos_(2)+1:pos_(3)-1));
  rep = str2num(fName(pos_(3)+1:pos_(4)-1));
  % construct list of trainLengths
  if isempty(find(trainLengthVec==trainLength))
    trainLengthVec = [trainLengthVec trainLength];
  end
  % load model data
  load(['../nls_data/expResults/',expCode,'/',fName])
  % name for struct fields
  tL = ['tL_',num2str(trainLength)];
  %% gather the summary results for best MSE
  if isfield(cvBest,'selected')
    selected(rep).(tL) = cvBest.selected;
  else
    selected(rep).(tL) = NaN(size(cvBest.sparsityTrain));
  end
  if isfield(cvBest,'iter')
    numIter(rep).(tL) = cvBest.iter;
  else
    numIter(rep).(tL) = [];
  end
  elapsedTime(rep).(tL) = cvBest.elapsedTime;
  minParamIdx(rep).(tL) = cvBest.minParamIdx;
  parameterGrid(rep).(tL) = paramGrid;
  mseTrain(rep).(tL) = cvBest.mseTrain;
  sparsityTrain(rep).(tL) = cvBest.sparsityTrain;
  mseValidation(rep).(tL) = cvBest.mseVal;
  sparsityValidation(rep).(tL) = cvBest.sparsityVal;
  %% gather the summary results for best sparsity near MSE
  if isfield(cvBestSp,'selected')
    selectedSp(rep).(tL) = cvBestSp.selected;
  else
    selectedSp(rep).(tL) = NaN(size(cvBestSp.sparsityTrain));
  end
  if isfield(cvBestSp,'iter')
    numIterSp(rep).(tL) = cvBestSp.iter;
  else
    numIterSp(rep).(tL) = [];
  end
  elapsedTimeSp(rep).(tL) = cvBestSp.elapsedTime;
  minParamIdxSp(rep).(tL) = cvBestSp.minParamIdx;
  parameterGridSp(rep).(tL) = paramGrid;
  mseTrainSp(rep).(tL) = cvBestSp.mseTrain;
  sparsityTrainSp(rep).(tL) = cvBestSp.sparsityTrain;
  mseValidationSp(rep).(tL) = cvBestSp.mseVal;
  sparsityValidationSp(rep).(tL) = cvBestSp.sparsityVal;
  mseTolSp(rep).(tL) = cvBestSp.mseTol;
end

% save the summaries
save(['../nls_data/expResults/',expCode,'/validSummary_',algo,'_',kernelType,num2str(kernelParam),'.mat'], ...
  'selected','numIter','elapsedTime','minParamIdx','parameterGrid','mseTrain','sparsityTrain','mseValidation','sparsityValidation')
save(['../nls_data/expResults/',expCode,'/validSummary_',algo,'_',kernelType,num2str(kernelParam),'.mat'], ...
  'selectedSp','numIterSp','elapsedTimeSp','minParamIdxSp','parameterGridSp','mseTrainSp','sparsityTrainSp','mseValidationSp','sparsityValidationSp','mseTolSp','-append')





end





