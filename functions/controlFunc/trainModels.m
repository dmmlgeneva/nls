function trainModels(expCode,trainLength,rep,algo,kernelType,kernelParam,varargin)
% TRAINMODELS - train models for various algos
%
% INPUTS
%   expCode - code of the experiment
%   trainLength - length of train data
%   rep - data replication number
%   kernelType - 'poly','polyHom' or 'Gauss'
%   kernelParam - degree of polynomials or sigma for Gauss
%   algo - code of the algo to use
%
% EXAMPLE: trainModels('exp002',50,2,'GL','poly',2,0,1,3,[ones(1,4),2*ones(1,4),3*ones(1,4),4*ones(1,4),5*ones(1,4),])
%
% CREATED: MG - 25/05/2017
% MG 23/09/2017 : overwrite temporary matrices to save memory
% MG 7/10/2017: merge L1 and L1H code so that L1 is L1(zero) (same for GL)
% MG 7/10/2017: change of logic in the ADMM solving

%% correct var types if deployed
if isdeployed
  trainLength = str2num(trainLength); rep = str2num(rep);
  kernelParam = str2num(kernelParam);
  for i = 1:nargin-6
    varargin{i} = str2num(varargin{i});
  end
  [~,hn] = system('hostname');
  if strcmp(hn(1:4),'node')
    cd /home/gregorom/nls ; % on baobab
  else
    cd /user/ai2/gregoma0/nls; % for testing on whales
  end
else
  cd /home/magda/Bitbucket/nls; % for testing on local machine
end

% create the results directory
mkdir(['../nls_data/expResults/',expCode])

%% select algo
switch algo
  case 'L1' % my L1 algo
    % get tau grid and mu
    if nargin == 10
      tauGrid.min = varargin{1}; 
      tauGrid.max = varargin{2}; 
      tauGrid.length = varargin{3}; 
      mu = varargin{4};
    else
      error('Wrong param grid and mu')
    end
    % get kernel matrices
    [matrices.K,Da,~,Lab,y] = getKernels(expCode,'train',trainLength,rep,kernelType,kernelParam);
    % precalculate algo inputs
    n = size(matrices.K,2); d = size(Da,3);
    MaTemp = permute(Da,[1,3,2]); D = reshape(MaTemp,[],n);
    MaTemp = permute(Lab,[1,2,4,3]); La = reshape(MaTemp,n,[],d);
    MaTemp = permute(Lab,[1,3,2,4]); Lb = reshape(MaTemp,[],n,d);
    MaTemp = permute(La,[1,3,2]); L = reshape(MaTemp,[],n*d);
    matrices.F = [matrices.K D'];
    Za = cat(2,Da,La); ZaTemp = permute(Za,[1,3,2]); 
    matrices.Z = reshape(ZaTemp,[],n+n*d);
    matrices.ZTZ = matrices.Z'*matrices.Z;
    if mu~=0
      matrices.Q = [matrices.K zeros(n,n*d); 2*D L];
    else
      matrices.Q = sparse(n+n*d,n+n*d);
    end
    % precalculate left- and right-hand sides of the optim problem
    matrices.LHS = 2*(matrices.F'*matrices.F)./n + mu*(matrices.Q'+matrices.Q);
    matrices.RHS = 2*matrices.F'*y./n;
    % train algo (for the full regularization path)
    [a,b,selected,convergenceHistory,elapsedTime] = trainRegPath_L1(matrices,y,mu,tauGrid,0);
    % get mse over train and the derivative norms
    [mseTrain,sparsityTrain] = getMse(y,cell2mat(a),cell2mat(b),matrices.K,D,Da,Lb);
    % and save
    tic
    save(['../nls_data/expResults/',expCode,'/model_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'mseTrain','sparsityTrain','a','b','selected','convergenceHistory','elapsedTime','tauGrid','mu')
    toc

  case 'eNet' % my eNet algo
    % get tau grid
    if nargin == 11
      tauGrid.min = varargin{1}; 
      tauGrid.max = varargin{2}; 
      tauGrid.length = varargin{3}; 
      mu = varargin{4};%watch out these are not named as in the paper
      muENet = varargin{5};
    else
      error('Wrong param grid and mu')
    end
    % get kernel matrices
    [matrices.K,Da,~,Lab,y] = getKernels(expCode,'train',trainLength,rep,kernelType,kernelParam);
    % precalculate algo inputs
    n = size(matrices.K,2); d = size(Da,3);
    MaTemp = permute(Da,[1,3,2]); D = reshape(MaTemp,[],n);
    MaTemp = permute(Lab,[1,2,4,3]); La = reshape(MaTemp,n,[],d);
    MaTemp = permute(Lab,[1,3,2,4]); Lb = reshape(MaTemp,[],n,d);
    LaTemp = permute(La,[1,3,2]); L = reshape(LaTemp,[],n*d);
    matrices.F = [matrices.K D'];
    Za = cat(2,Da,La); ZaTemp = permute(Za,[1,3,2]); 
    matrices.Z = reshape(ZaTemp,[],n+n*d);
    matrices.ZTZ = matrices.Z'*matrices.Z;
    if mu~=0
      matrices.Q = [matrices.K zeros(n,n*d); 2*D L];
    else
      matrices.Q = sparse(n+n*d,n+n*d);
    end
    % precalculate left- and right-hand sides of the optim problem
    matrices.LHS = 2*(matrices.F'*matrices.F)./n + mu*(matrices.Q'+matrices.Q);
    matrices.RHS = 2*matrices.F'*y./n;
    % train algo (for the full regularization path)
    [a,b,selected,convergenceHistory,elapsedTime] = trainRegPath_eNet(matrices,y,mu,muENet,tauGrid,0);
    % get mse over train and the derivative norms
    [mseTrain,sparsityTrain] = getMse(y,cell2mat(a),cell2mat(b),matrices.K,D,Da,Lb);
    % and save
    save(['../nls_data/expResults/',expCode,'/model_',algo,'(',num2str(mu),'_',num2str(muENet),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'mseTrain','sparsityTrain','a','b','selected','convergenceHistory','elapsedTime','tauGrid')

  case 'GL' % my GL algo
    if nargin < 11
      error('Wrong grid or group assignment')
    end
    % get tau grid  
    tauGrid.min = varargin{1}; 
    tauGrid.max = varargin{2}; 
    tauGrid.length = varargin{3}; 
    mu = varargin{4};
    % get group assignments;
    grpAssign = [varargin{5:end}]';
    grpIds = unique(grpAssign);
    numGrps = numel(grpIds);
    % get kernel matrices
    [matrices.K,Da,~,Lab,y] = getKernels(expCode,'train',trainLength,rep,kernelType,kernelParam);
    % precalculate algo inputs
    n = size(matrices.K,2); d = size(Da,3);
    MaTemp = permute(Da,[1,3,2]); D = reshape(MaTemp,[],n);
    MaTemp = permute(Lab,[1,2,4,3]); La = reshape(MaTemp,n,[],d);
    MaTemp = permute(Lab,[1,3,2,4]); Lb = reshape(MaTemp,[],n,d);
    MaTemp = permute(La,[1,3,2]); L = reshape(MaTemp,[],n*d);
    matrices.F = [matrices.K D'];
    Za = cat(2,Da,La); ZaTemp = permute(Za,[1,3,2]); 
    matrices.Z = reshape(ZaTemp,[],n+n*d);
    matrices.ZTZ = matrices.Z'*matrices.Z;
    if mu~=0
      matrices.Q = [matrices.K zeros(n,n*d); 2*D L];
    else
      matrices.Q = sparse(n+n*d,n+n*d);
    end
    % precalculate left- and right-hand sides of the optim problem
    matrices.LHS = 2*(matrices.F'*matrices.F)./n + mu*(matrices.Q'+matrices.Q);
    matrices.RHS = 2*matrices.F'*y./n;
    for gIdx=1:numGrps
      MaTemp = Da(:,:,find(grpAssign==grpIds(gIdx)));
      MaTemp = permute(MaTemp,[1,3,2]); DG = reshape(MaTemp,[],n);
      MaTemp = La(:,:,find(grpAssign==grpIds(gIdx)));
      MaTemp = permute(MaTemp,[1,3,2]); LG = reshape(MaTemp,[],n*d);
      matrices.ZG{gIdx} = [DG LG];
    end
    % train algo (for the full regularization path)
    [a,b,selected,convergenceHistory,elapsedTime] = trainRegPath_GL(matrices,grpAssign,y,mu,tauGrid,0);
    % get mse over train and the derivative norms
    [mseTrain,sparsityTrain] = getMse(y,cell2mat(a),cell2mat(b),matrices.K,D,Da,Lb);
    % and save
    save(['../nls_data/expResults/',expCode,'/model_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'mseTrain','sparsityTrain','a','b','selected','convergenceHistory','elapsedTime','tauGrid','mu','grpAssign')

  case 'denovas1' % Rosasco denovas in 1 step
    % get tau grid and mu
    if nargin == 10
      tauGrid.min = varargin{1}; 
      tauGrid.max = varargin{2}; 
      tauGrid.length = varargin{3}; 
      mu = varargin{4};
    else
      error('Wrong param grid and mu')
    end
    % get kernel matrices
    [data.K,data.Z,data.L,data.Y] = getKernelsDenovas(expCode,trainLength,rep,kernelType,kernelParam);
    % train algo (for the full regularization path)
    [a,b,selected,numIter,elapsedTime] = trainRegPath_denovas1(data,mu,tauGrid,0);
    % get mse over train and the derivative norms
    [K,Da,~,Lab,y] = getKernels(expCode,'train',trainLength,rep,kernelType,kernelParam);
    n = size(K,2); d = size(Da,3);
    MaTemp = permute(Da,[1,3,2]); D = reshape(MaTemp,[],n);
    MaTemp = permute(Lab,[1,3,2,4]); Lb = reshape(MaTemp,[],n,d);
    [mseTrain,sparsityTrain] = getMse(y,cell2mat(a),cell2mat(b),K,D,Da,Lb);
    % and save
    save(['../nls_data/expResults/',expCode,'/model_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'mseTrain','sparsityTrain','a','b','selected','numIter','elapsedTime','tauGrid','mu')

  case 'krls' % standard krls (hilbert norm regularized least squares)
    % get tau grid
    if nargin == 9
      tauGrid.min = varargin{1}; 
      tauGrid.max = varargin{2}; 
      tauGrid.length = varargin{3}; 
    else
      error('Wrong tau grid')
    end
    % get kernel matrices
    [K,Da,~,Lab,y] = getKernels(expCode,'train',trainLength,rep,kernelType,kernelParam);
    n = size(K,2); d = size(Da,3);
    MaTemp = permute(Da,[1,3,2]); D = reshape(MaTemp,[],n);
    MaTemp = permute(Lab,[1,3,2,4]); Lb = reshape(MaTemp,[],n,d);
    % train algo (for the full regularization path)
    [a,elapsedTime] = trainRegPath_krls(K,y,tauGrid,0);
    % get mse over train and the derivative norms
    [mseTrain,sparsityTrain] = getMse(y,cell2mat(a),zeros(n*d,tauGrid.length),K,D,Da,Lb);
    % and save
    save(['../nls_data/expResults/',expCode,'/model_',algo,'_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'mseTrain','sparsityTrain','a','elapsedTime','tauGrid')

  case 'LinL1' % liner L1 algo as a baseline
    % get tau grid
    if nargin == 9
      tauGrid.min = varargin{1}; 
      tauGrid.max = varargin{2}; 
      tauGrid.length = varargin{3}; 
    else
      error('Wrong tau grid')
    end
    % train algo (for the full regularization path)
    load(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep)],'x','y')
    [a,selected,convergenceHistory,elapsedTime] = trainRegPath_LinL1(x,y,tauGrid,0);
    % get mse over train 
    sparsityTrain = selected; %(for consistency with nonlinear methods)
    errVals = bsxfun(@minus,x*cell2mat(a),y);
    mseTrain = sum(errVals.^2)./size(x,1); % for each model in a column
    % and save
    save(['../nls_data/expResults/',expCode,'/model_',algo,'_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'mseTrain','sparsityTrain','a','selected','convergenceHistory','elapsedTime','tauGrid')

  case 'LinENet' % liner eNet algo as a baseline
    % get tau grid
    if nargin == 10
      tauGrid.min = varargin{1}; 
      tauGrid.max = varargin{2}; 
      tauGrid.length = varargin{3}; 
      mu = varargin{4};
    else
      error('Wrong hyper-params')
    end
    % train algo (for the full regularization path)
    load(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep)],'x','y')
    [a,selected,convergenceHistory,elapsedTime] = trainRegPath_LinENet(x,y,mu,tauGrid,0);
    % get mse over train 
    sparsityTrain = selected; %(for consistency with nonlinear methods)
    errVals = bsxfun(@minus,x*cell2mat(a),y);
    mseTrain = sum(errVals.^2)./size(x,1); % for each model in a column
    % and save
    save(['../nls_data/expResults/',expCode,'/model_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'mseTrain','sparsityTrain','a','selected','convergenceHistory','elapsedTime','tauGrid','mu')

    
    
  case 'LinGL' % liner GL algo as a baseline
    if nargin < 10
      error('Wrong grid or group assignment')
    end
    % get tau grid  
    tauGrid.min = varargin{1}; 
    tauGrid.max = varargin{2}; 
    tauGrid.length = varargin{3}; 
    % get group assignments;
    grpAssign = [varargin{4:end}]';
    grpIds = unique(grpAssign);
    numGrps = numel(grpIds);
    % train algo (for the full regularization path)
    load(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep)],'x','y')
    [a,selected,convergenceHistory,elapsedTime] = trainRegPath_LinGL(x,grpAssign,y,tauGrid,0);
    % get mse over train 
    sparsityTrain = selected; %(for consistency with nonlinear methods)
    errVals = bsxfun(@minus,x*cell2mat(a),y);
    mseTrain = sum(errVals.^2)./size(x,1); % for each model in a column
    % and save
    save(['../nls_data/expResults/',expCode,'/model_',algo,'_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'mseTrain','sparsityTrain','a','selected','convergenceHistory','elapsedTime','tauGrid','grpAssign')
  
  case 'mean' % mean model as a baseline
    % get tau grid
    if nargin == 9
      tauGrid.min = varargin{1}; 
      tauGrid.max = varargin{2}; 
      tauGrid.length = varargin{3}; 
    else
      error('Wrong tau grid')
    end
    % train algo (for the full regularization path)
    load(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep)],'x','y')
    % train algo (for the full regularization path)
    [a,elapsedTime] = trainRegPath_mean(y,tauGrid,0);
    % get mse over train
    sparsityTrain = ones(size(x,2),1);
    errVals = bsxfun(@minus,repmat(cell2mat(a),size(y,1),1),y);
    mseTrain = sum(errVals.^2)./size(x,1); % for each model in a column
    save(['../nls_data/expResults/',expCode,'/model_',algo,'_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'mseTrain','sparsityTrain','a','elapsedTime','tauGrid')
    
  otherwise
  error('Unknown algo')
end


end





