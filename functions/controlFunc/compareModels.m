function compareModels(expCode,varargin)
% COMPAREMODELS - compare model results across algorithms
%   use cross-validation based on MSE
%
% INPUTS
%   expCode - code of the experiment
%   varargin - structs with algo, kernelType and kernelParam 
%     or just [kernelType kernelParam], e.g. polyHom2
%
% EXAMPLE: compareModels('exp001',mod1,mod2)
%
% CREATED: MG - 18/05/2017

%% correct folders 
[~,hn] = system('hostname');
switch hn(1:4)
  case 'node'
    cd /home/gregorom/nls ; % on baobab
  case 'magd'
    cd /home/magda/Bitbucket/nls; % for testing on local machine
  case 'unig'
    cd /home/magda/Bitbucket/nls; % for testing on local machine
  otherwise
    cd /user/ai2/gregoma0/nls; % on whales
end

%% get the experimenta sparsity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch expCode
  case 'exp001'
    nonIdx = logical([zeros(4,1); ones(16,1)]);
  case 'exp002'
    nonIdx = logical([zeros(4,1); ones(16,1)]);
  case 'exp003'
    nonIdx = logical([zeros(4,1); ones(16,1)]);
  case 'exp004'
    nonIdx = logical([zeros(6,1); ones(6,1)]);
  case 'exp005'
    nonIdx = logical([zeros(6,1); ones(6,1)]);
  case 'exp006'
    nonIdx = logical([zeros(8,1); ones(4,1)]);
  case 'exp007'
    nonIdx = logical([zeros(6,1); ones(6,1)]);
  case 'exp008'
    nonIdx = logical([zeros(6,1); ones(6,1)]);
  otherwise
    nonIdx = [];
end

%% either for a list of algos or for algos for which we have results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kernelSpec='';
if nargin>1 && isfield(varargin{1},'algo') % if given list of algos
  for mIdx = 1:nargin-1
    algo{mIdx}=varargin{mIdx}.algo;
    kernel{mIdx} = [varargin{mIdx}.kernelType,num2str(varargin{mIdx}.kernelParam)];
  end
else
  if nargin > 1 % create list of algos from results directly
    kernelSpec=varargin{1};
  end
  fList = dir(['../nls_data/expResults/',expCode,'/testSummary*']); fList = {fList.name}';
  fNum = length(fList);
  if ~(length(fList)>0)
    error('No test results available for experiment %s',expCode)
  end
  pIdx = 0;
  for fIdx=1:fNum
    fName = fList{fIdx};
    % get separators
    pos_ = strfind(fName,'_');
    kr = fName(pos_(2)+1:end-4);
    % keep going only if results for the correct kernel
    if ~isempty(kernelSpec) && isempty(strfind(fName,kernelSpec))
      continue
    else
      pIdx = pIdx + 1;
      algo{pIdx} = fName(pos_(1)+1:pos_(2)-1);
      kernel{pIdx} = kr;
    end

  end
end

%% initialize printing
mkdir(['../nls_data/expPrints/',expCode])
%outFile = ['../nls_data/expPrints/',expCode,'/testResults_',datestr(now,'yyyymmdd_HHMMSS'),'.csv'];
outFile = ['../nls_data/expPrints/',expCode,'/testResults_',kernelSpec,'.csv'];
fileID = fopen(outFile, 'w');
%% print heading
fprintf(fileID,'Test results %s %s \n \n',expCode, datestr(now,'dd/mm/yyyy HH:MM:SS'));

%% print results for all algos 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%figMSE = figure;
for mIdx = 1:numel(algo)
  fprintf('processing algo %s \n',algo{mIdx})
  %% load the summaries
  load(['../nls_data/expResults/',expCode,'/testSummary_',algo{mIdx},'_',kernel{mIdx},'.mat'])
  
  %% get avg mseTest
  avgMseTest =  mean(reshape(cell2mat(struct2cell(mseTest)),[],size(mseTest,2)),2);
  % order by train size
  fS = fields(mseTest);
  [trVec,fIdx] = sort(cellfun(@(x) str2num(x(4:end)),fS));
  avgMseTest = avgMseTest(fIdx);
  
  %% print avgMse to csv
  if mIdx==1
    fprintf(fileID,['%s', repmat([', %d'],1,length(trVec)), ', , ' ],'MSE',trVec);
    title('Average mse')
    fprintf(fileID,['%s', repmat([', %d'],1,length(trVec)) '\n'],'nonActive',trVec);
    title('nonActive norm')
  end
  algoName{mIdx} = [algo{mIdx},'_',kernel{mIdx}];
  fprintf(fileID,['%s', repmat([', %f'],1,length(trVec)), ', , ' ],algoName{mIdx},avgMseTest);  

  % get avg sparsityTest
  sparsityTest = reshape(mean(cell2mat(squeeze(struct2cell(sparsityTest))),2),[],size(avgMseTest,1));
  sparsityTest = sparsityTest(:,fIdx); % reorder according to trainLength
  sparsityTest = bsxfun(@rdivide, sparsityTest, sum(sparsityTest)); % and normalize to sum to 1 across all derivatives
  nonActive = sparsityTest(nonIdx,:);
  avgNActNorm = sum(abs(nonActive));
  
  %% print avgMse to csv
  algoName{mIdx} = [algo{mIdx},'_',kernel{mIdx}];
  fprintf(fileID,['%s', repmat([', %f'],1,length(trVec)) '\n'],algoName{mIdx},avgNActNorm);  

end


end





