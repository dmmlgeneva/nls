function validateModels(expCode,trainLength,rep,algo,kernelType,kernelParam,varargin)
% VALIDATEMODELS - validate models for various algos, cvBest has the
% selected models based on mse, cvBestSp has the selected models
% near(+-cvBestSp.mseTol) with lowest norms across nonactive dimensions
% obviously, this is only reasonable for synthetic experiments where we know the sparsity
%
% INPUTS
%   expCode - code of the experiment
%   trainLength - length of train data
%   rep - data replication number
%   algo - code of the algo to use
%   kernelType - 'poly','polyHom' or 'Gauss'
%   kernelParam - degree of polynomials or sigma for Gauss
%   varargin - mu for denovas1
%
% EXAMPLE: validateModels('exp002',50,2,'eNet','poly',4,0.7)
%
% CREATED: MG - 15/05/2017
% MG 11/09/2017: if validate is trainSample specific, load that instead of general single validate for all trainSamples
% MG 7/10/2017: merge L1 and L1H code so that L1 is L1(zero) (same for GL)

%% correct var types if deployed
if isdeployed
  trainLength = str2num(trainLength); rep = str2num(rep);
  kernelParam = str2num(kernelParam); 
  if nargin>6
    for varIdx = 1:nargin-6
      varargin{varIdx} = str2num(varargin{varIdx});
    end
  end
  [~,hn] = system('hostname');
  if strcmp(hn(1:4),'node')
    cd /home/gregorom/nls ; % on baobab
  else
    cd /user/ai2/gregoma0/nls; % for testing on whales
  end
else
  cd /home/magda/Bitbucket/nls; % for testing on local machine
end

%% get the experimental sparsity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch expCode
  case 'exp001'
    nonIdx = logical([zeros(4,1); ones(16,1)]);
  case 'exp002'
    nonIdx = logical([zeros(4,1); ones(16,1)]);
  case 'exp003'
    nonIdx = logical([zeros(4,1); ones(16,1)]);
  case 'exp004'
    nonIdx = logical([zeros(6,1); ones(6,1)]);
  case 'exp005'
    nonIdx = logical([zeros(6,1); ones(6,1)]);
  otherwise
    nonIdx = [];
end

%% mse tolerance for sparsity search
cvBestSp.mseTol = 1e-3;


%% select algo
switch algo
  case 'L1' % my L1H algo
    mu = varargin{1};
    % get kernel matrices
    [K,Da,Da1,Lab,y] = getKernels(expCode,'valid',trainLength,rep,kernelType,kernelParam);
    % precalculate summary matrices
    n = size(K,2); d = size(Da,3);
    DaTemp = permute(Da,[1,3,2]); D = reshape(DaTemp,[],n);
    LabTemp = permute(Lab,[1,3,2,4]); Lb = reshape(LabTemp,[],n,d);
    % get mse over test and the derivative norms
    load(['../nls_data/expResults/',expCode,'/model_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'a','b','selected','convergenceHistory','elapsedTime','tauGrid','mseTrain','sparsityTrain')
    [mseVal,sparsityVal,stdErrorVal] = getMse(y,cell2mat(a),cell2mat(b),K,D,Da1,Lb); % watchout, need to pass in Da1 matrix to have the derivatives with respect to validation set
    paramGrid = tauGrid;
    % find the best hyper-param values
    [~,minParamIdx] = min(mseVal);
    cvBest.a = a{minParamIdx}; cvBest.b = b{minParamIdx}; cvBest.selected = selected{minParamIdx};
    cvBest.iter = size(convergenceHistory{minParamIdx},1);
    cvBest.elapsedTime = elapsedTime{minParamIdx};
    cvBest.minParamIdx = minParamIdx;
    cvBest.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBest.mseTrain = mseTrain(minParamIdx);
    cvBest.mseVal = mseVal(minParamIdx);
    cvBest.sparsityVal = sparsityVal(:,minParamIdx);
    %% get the best sparsity near the best mse
    nonActive = sparsityVal(nonIdx,:);
    avgNActNorm = sum(abs(nonActive));
    minNorm = min(avgNActNorm(abs(mseVal-cvBest.mseVal)<=cvBestSp.mseTol));
    minParamIdx = min(find(avgNActNorm==minNorm)); % extra min in case there are more with the same norm
    cvBestSp.a = a{minParamIdx}; cvBestSp.b = b{minParamIdx}; cvBestSp.selected = selected{minParamIdx};
    cvBestSp.iter = size(convergenceHistory{minParamIdx},1);
    cvBestSp.elapsedTime = elapsedTime{minParamIdx};
    cvBestSp.minParamIdx = minParamIdx;
    cvBestSp.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBestSp.mseTrain = mseTrain(minParamIdx);
    cvBestSp.mseVal = mseVal(minParamIdx);
    cvBestSp.sparsityVal = sparsityVal(:,minParamIdx);
    % and save
    tic
    save(['../nls_data/expResults/',expCode,'/valid_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'mseVal','sparsityVal','paramGrid','cvBest','cvBestSp')
    toc
   
  case 'eNet' % my elastic Net algo
    mu = varargin{1};
    muENet = varargin{2};
    % get kernel matrices
    [K,Da,Da1,Lab,y] = getKernels(expCode,'valid',trainLength,rep,kernelType,kernelParam);
    % precalculate summary matrices
    n = size(K,2); d = size(Da,3);
    DaTemp = permute(Da,[1,3,2]); D = reshape(DaTemp,[],n);
    LabTemp = permute(Lab,[1,3,2,4]); Lb = reshape(LabTemp,[],n,d);
    %% 1) get mse over test and the derivative norms (for naive eNet)
    load(['../nls_data/expResults/',expCode,'/model_',algo,'(',num2str(mu),'_',num2str(muENet),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'a','b','selected','convergenceHistory','elapsedTime','tauGrid','mseTrain','sparsityTrain')
    [mseVal,sparsityVal] = getMse(y,cell2mat(a),cell2mat(b),K,D,Da1,Lb); % watchout, need to pass in Da1 matrix to have the derivatives with respect to validation set
    paramGrid = tauGrid;
    % find the best hyper-param values
    [~,minParamIdx] = min(mseVal);
    cvBest.a = a{minParamIdx}; cvBest.b = b{minParamIdx}; cvBest.selected = selected{minParamIdx};
    cvBest.iter = size(convergenceHistory{minParamIdx},1);
    cvBest.elapsedTime = elapsedTime{minParamIdx};
    cvBest.minParamIdx = minParamIdx;
    cvBest.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBest.mseTrain = mseTrain(minParamIdx);
    cvBest.mseVal = mseVal(minParamIdx);
    cvBest.sparsityVal = sparsityVal(:,minParamIdx);
    %% get the best sparsity near the best mse
    nonActive = sparsityVal(nonIdx,:);
    avgNActNorm = sum(abs(nonActive));
    minNorm = min(avgNActNorm(abs(mseVal-cvBest.mseVal)<=cvBestSp.mseTol));
    minParamIdx = min(find(avgNActNorm==minNorm)); % extra min in case there are more with the same norm
    cvBestSp.a = a{minParamIdx}; cvBestSp.b = b{minParamIdx}; cvBestSp.selected = selected{minParamIdx};
    cvBestSp.iter = size(convergenceHistory{minParamIdx},1);
    cvBestSp.elapsedTime = elapsedTime{minParamIdx};
    cvBestSp.minParamIdx = minParamIdx;
    cvBestSp.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBestSp.mseTrain = mseTrain(minParamIdx);
    cvBestSp.mseVal = mseVal(minParamIdx);
    cvBestSp.sparsityVal = sparsityVal(:,minParamIdx);
    % and save
    tic
    save(['../nls_data/expResults/',expCode,'/valid_',algo,'(',num2str(mu),'_',num2str(muENet),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'mseVal','sparsityVal','paramGrid','cvBest','cvBestSp')
    toc
    %% 2) get the mse and sparsity for rescaled eNet (see technical notes)
    rescaleGrid = logspace(tauGrid.min,tauGrid.max,tauGrid.length).*(1-muENet);
    aResc = bsxfun(@times,cell2mat(a),rescaleGrid);
    bResc = bsxfun(@times,cell2mat(b),rescaleGrid);  
    [mseVal,sparsityVal] = getMse(y,aResc,bResc,K,D,Da1,Lb); % watchout, need to pass in Da1 matrix to have the derivatives with respect to validation set
    % find the best hyper-param values
    [~,minParamIdx] = min(mseVal);
    cvBest.a = aResc(:,minParamIdx); cvBest.b = bResc(:,minParamIdx); cvBest.selected = selected{minParamIdx};
    cvBest.iter = size(convergenceHistory{minParamIdx},1);
    cvBest.elapsedTime = elapsedTime{minParamIdx};
    cvBest.minParamIdx = minParamIdx;
    cvBest.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBest.mseTrain = mseTrain(minParamIdx);
    cvBest.mseVal = mseVal(minParamIdx);
    cvBest.sparsityVal = sparsityVal(:,minParamIdx);
    %% get the best sparsity near the best mse
    nonActive = sparsityVal(nonIdx,:);
    avgNActNorm = sum(abs(nonActive));
    minNorm = min(avgNActNorm(abs(mseVal-cvBest.mseVal)<=cvBestSp.mseTol));
    minParamIdx = min(find(avgNActNorm==minNorm)); % extra min in case there are more with the same norm
    cvBestSp.a = aResc(:,minParamIdx); cvBestSp.b = bResc(:,minParamIdx); cvBestSp.selected = selected{minParamIdx};
    cvBestSp.iter = size(convergenceHistory{minParamIdx},1);
    cvBestSp.elapsedTime = elapsedTime{minParamIdx};
    cvBestSp.minParamIdx = minParamIdx;
    cvBestSp.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBestSp.mseTrain = mseTrain(minParamIdx);
    cvBestSp.mseVal = mseVal(minParamIdx);
    cvBestSp.sparsityVal = sparsityVal(:,minParamIdx);
    % and save
    tic
    save(['../nls_data/expResults/',expCode,'/valid_',algo,'(',num2str(mu),'_',num2str(muENet),')Resc','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'mseVal','sparsityVal','paramGrid','cvBest','cvBestSp')
    toc
 
  case 'GL' % my GLH algo
    mu = varargin{1};
    % get kernel matrices
    [K,Da,Da1,Lab,y] = getKernels(expCode,'valid',trainLength,rep,kernelType,kernelParam);
    % precalculate summary matrices
    n = size(K,2); d = size(Da,3);
    DaTemp = permute(Da,[1,3,2]); D = reshape(DaTemp,[],n);
    LabTemp = permute(Lab,[1,3,2,4]); Lb = reshape(LabTemp,[],n,d);
    % get mse over test and the derivative norms
    load(['../nls_data/expResults/',expCode,'/model_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'a','b','selected','convergenceHistory','elapsedTime','tauGrid','mseTrain','sparsityTrain')
    [mseVal,sparsityVal] = getMse(y,cell2mat(a),cell2mat(b),K,D,Da1,Lb); % watchout, need to pass in Da1 matrix to have the derivatives with respect to validation set
    paramGrid = tauGrid;
    % find the best hyper-param values
    [~,minParamIdx] = min(mseVal);
    cvBest.a = a{minParamIdx}; cvBest.b = b{minParamIdx}; cvBest.selected = selected{minParamIdx};
    cvBest.iter = size(convergenceHistory{minParamIdx},1);
    cvBest.elapsedTime = elapsedTime{minParamIdx};
    cvBest.minParamIdx = minParamIdx;
    cvBest.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBest.mseTrain = mseTrain(minParamIdx);
    cvBest.mseVal = mseVal(minParamIdx);
    cvBest.sparsityVal = sparsityVal(:,minParamIdx);
    %% get the best sparsity near the best mse
    nonActive = sparsityVal(nonIdx,:);
    avgNActNorm = sum(abs(nonActive));
    minNorm = min(avgNActNorm(abs(mseVal-cvBest.mseVal)<=cvBestSp.mseTol));
    minParamIdx = min(find(avgNActNorm==minNorm)); % extra min in case there are more with the same norm
    cvBestSp.a = a{minParamIdx}; cvBestSp.b = b{minParamIdx}; cvBestSp.selected = selected{minParamIdx};
    cvBestSp.iter = size(convergenceHistory{minParamIdx},1);
    cvBestSp.elapsedTime = elapsedTime{minParamIdx};
    cvBestSp.minParamIdx = minParamIdx;
    cvBestSp.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBestSp.mseTrain = mseTrain(minParamIdx);
    cvBestSp.mseVal = mseVal(minParamIdx);
    cvBestSp.sparsityVal = sparsityVal(:,minParamIdx);
    % and save
    tic
    save(['../nls_data/expResults/',expCode,'/valid_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'mseVal','sparsityVal','paramGrid','cvBest','cvBestSp')
    toc

  case 'denovas1' % Rosasco denovas in 1 step
    mu = varargin{1};
    % get kernel matrices - do not need the special Denovas code here cause
    % once the parameters are calculated they are the same as our model
    % would give
    [K,Da,Da1,Lab,y] = getKernels(expCode,'valid',trainLength,rep,kernelType,kernelParam);
    % precalculate summary matrices
    n = size(K,2); d = size(Da,3);
    DaTemp = permute(Da,[1,3,2]); D = reshape(DaTemp,[],n);
    LabTemp = permute(Lab,[1,3,2,4]); Lb = reshape(LabTemp,[],n,d);
    % get mse over test and the derivative norms
    load(['../nls_data/expResults/',expCode,'/model_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'a','b','selected','numIter','elapsedTime','tauGrid','mseTrain','sparsityTrain')
    [mseVal,sparsityVal] = getMse(y,cell2mat(a),cell2mat(b),K,D,Da1,Lb); % watchout, need to pass in Da1 matrix to have the derivatives with respect to validation set
    paramGrid = tauGrid;
    % find the best hyper-param values
    [~,minParamIdx] = min(mseVal);
    cvBest.a = a{minParamIdx}; cvBest.b = b{minParamIdx}; cvBest.selected = selected{minParamIdx};
    cvBest.iter = numIter{minParamIdx};
    cvBest.elapsedTime = elapsedTime{minParamIdx};
    cvBest.minParamIdx = minParamIdx;
    cvBest.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBest.mseTrain = mseTrain(minParamIdx);
    cvBest.mseVal = mseVal(minParamIdx);
    cvBest.sparsityVal = sparsityVal(:,minParamIdx);
    %% get the best sparsity near the best mse
    nonActive = sparsityVal(nonIdx,:);
    avgNActNorm = sum(abs(nonActive));
    minNorm = min(avgNActNorm(abs(mseVal-cvBest.mseVal)<=cvBestSp.mseTol));
    minParamIdx = min(find(avgNActNorm==minNorm)); % extra min in case there are more with the same norm
    cvBestSp.a = a{minParamIdx}; cvBestSp.b = b{minParamIdx}; cvBestSp.selected = selected{minParamIdx};
    cvBestSp.iter = numIter{minParamIdx};
    cvBestSp.elapsedTime = elapsedTime{minParamIdx};
    cvBestSp.minParamIdx = minParamIdx;
    cvBestSp.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBestSp.mseTrain = mseTrain(minParamIdx);
    cvBestSp.mseVal = mseVal(minParamIdx);
    cvBestSp.sparsityVal = sparsityVal(:,minParamIdx);
    % and save
    tic
    save(['../nls_data/expResults/',expCode,'/valid_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'mseVal','sparsityVal','paramGrid','cvBest','cvBestSp')
    toc
  
  case 'krls' % standard krls (hilbert norm regularized least squares)
    % get kernel matrices
    [K,Da,Da1,Lab,y] = getKernels(expCode,'valid',trainLength,rep,kernelType,kernelParam);
    % precalculate summary matrices
    [nTr,n] = size(K); d = size(Da,3);
    DaTemp = permute(Da,[1,3,2]); D = reshape(DaTemp,[],n);
    LabTemp = permute(Lab,[1,3,2,4]); Lb = reshape(LabTemp,[],n,d);
    % get mse over test and the derivative norms
    load(['../nls_data/expResults/',expCode,'/model_',algo,'_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'a','elapsedTime','tauGrid','mseTrain','sparsityTrain')
    [mseVal,sparsityVal] = getMse(y,cell2mat(a),zeros(nTr*d,tauGrid.length),K,D,Da1,Lb); % watchout, need to pass in Da1 matrix to have the derivatives with respect to validation set
    paramGrid = tauGrid;
    % find the best hyper-param values
    [~,minParamIdx] = min(mseVal);
    cvBest.a = a{minParamIdx}; 
    cvBest.elapsedTime = elapsedTime{minParamIdx};
    cvBest.minParamIdx = minParamIdx;
    cvBest.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBest.mseTrain = mseTrain(minParamIdx);
    cvBest.mseVal = mseVal(minParamIdx);
    cvBest.sparsityVal = sparsityVal(:,minParamIdx);
    %% get the best sparsity near the best mse
    nonActive = sparsityVal(nonIdx,:);
    avgNActNorm = sum(abs(nonActive));
    minNorm = min(avgNActNorm(abs(mseVal-cvBest.mseVal)<=cvBestSp.mseTol));
    minParamIdx = min(find(avgNActNorm==minNorm)); % extra min in case there are more with the same norm
    cvBestSp.a = a{minParamIdx}; 
    cvBestSp.elapsedTime = elapsedTime{minParamIdx};
    cvBestSp.minParamIdx = minParamIdx;
    cvBestSp.sparsityTrain = sparsityTrain(:,minParamIdx);
    cvBestSp.mseTrain = mseTrain(minParamIdx);
    cvBestSp.mseVal = mseVal(minParamIdx);
    cvBestSp.sparsityVal = sparsityVal(:,minParamIdx);
    % and save
    tic
    save(['../nls_data/expResults/',expCode,'/valid_',algo,'_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'mseVal','sparsityVal','paramGrid','cvBest','cvBestSp')
    toc

  case 'LinL1' % liner L1 algo as a baseline
    % get mse over test and the derivative norms
    fName1 = ['../nls_data/expData/',expCode,'/validData',num2str(trainLength),'_',num2str(rep),'.mat'];
    fName2 = ['../nls_data/expData/',expCode,'/validData','_',num2str(rep),'.mat'];
    fName3 = ['../nls_data/expData/',expCode,'/validData.mat'];
    if exist(fName1, 'file') == 2
      val = load(fName1,'x','y');
    elseif exist(fName2, 'file') == 2
      val = load(fName2,'x','y');
    else
      val = load(fName3,'x','y');
    end
%    val = load(['../nls_data/expData/',expCode,'/validData'],'x','y');
    load(['../nls_data/expResults/',expCode,'/model_',algo,'_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'a','selected','convergenceHistory','elapsedTime','tauGrid','mseTrain','sparsityTrain')
    sparsityVal = cell2mat(sparsityTrain); %(for consistency with nonlinear methods)
    errVals = bsxfun(@minus,val.x*cell2mat(a),val.y);
    mseVal = sum(errVals.^2)./size(val.x,1); % for each model in a column
    paramGrid = tauGrid;
    % find the best hyper-param values
    [~,minParamIdx] = min(mseVal);
    cvBest.a = a{minParamIdx}; cvBest.selected = selected{minParamIdx};
    cvBest.iter = size(convergenceHistory{minParamIdx},1);
    cvBest.elapsedTime = elapsedTime{minParamIdx};
    cvBest.minParamIdx = minParamIdx;
    cvBest.sparsityTrain = sparsityTrain{:,minParamIdx};
    cvBest.mseTrain = mseTrain(minParamIdx);
    cvBest.mseVal = mseVal(minParamIdx);
    cvBest.sparsityVal = sparsityVal(:,minParamIdx);
    %% get the best sparsity near the best mse
    nonActive = sparsityVal(nonIdx,:);
    avgNActNorm = sum(abs(nonActive));
    minNorm = min(avgNActNorm(abs(mseVal-cvBest.mseVal)<=cvBestSp.mseTol));
    minParamIdx = min(find(avgNActNorm==minNorm)); % extra min in case there are more with the same norm
    cvBestSp.a = a{minParamIdx}; cvBestSp.selected = selected{minParamIdx};
    cvBestSp.iter = size(convergenceHistory{minParamIdx},1);
    cvBestSp.elapsedTime = elapsedTime{minParamIdx};
    cvBestSp.minParamIdx = minParamIdx;
    cvBestSp.sparsityTrain = sparsityTrain{:,minParamIdx};
    cvBestSp.mseTrain = mseTrain(minParamIdx);
    cvBestSp.mseVal = mseVal(minParamIdx);
    cvBestSp.sparsityVal = sparsityVal(:,minParamIdx);
    % and save
    tic
    save(['../nls_data/expResults/',expCode,'/valid_',algo,'_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'mseVal','sparsityVal','paramGrid','cvBest','cvBestSp')
    toc


    
  case 'LinENet' % liner eNet algo as a baseline
    mu = varargin{1};
    % get mse over test and the derivative norms
    fName1 = ['../nls_data/expData/',expCode,'/validData',num2str(trainLength),'_',num2str(rep),'.mat'];
    fName2 = ['../nls_data/expData/',expCode,'/validData','_',num2str(rep),'.mat'];
    fName3 = ['../nls_data/expData/',expCode,'/validData.mat'];
    if exist(fName1, 'file') == 2
      val = load(fName1,'x','y');
    elseif exist(fName2, 'file') == 2
      val = load(fName2,'x','y');
    else
      val = load(fName3,'x','y');
    end
%    val = load(['../nls_data/expData/',expCode,'/validData'],'x','y');
    load(['../nls_data/expResults/',expCode,'/model_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'a','selected','convergenceHistory','elapsedTime','tauGrid','mseTrain','sparsityTrain')
    sparsityVal = cell2mat(sparsityTrain); %(for consistency with nonlinear methods)
    errVals = bsxfun(@minus,val.x*cell2mat(a),val.y);
    mseVal = sum(errVals.^2)./size(val.x,1); % for each model in a column
    paramGrid = tauGrid;
    % find the best hyper-param values
    [~,minParamIdx] = min(mseVal);
    cvBest.a = a{minParamIdx}; cvBest.selected = selected{minParamIdx};
    cvBest.iter = size(convergenceHistory{minParamIdx},1);
    cvBest.elapsedTime = elapsedTime{minParamIdx};
    cvBest.minParamIdx = minParamIdx;
    cvBest.sparsityTrain = sparsityTrain{:,minParamIdx};
    cvBest.mseTrain = mseTrain(minParamIdx);
    cvBest.mseVal = mseVal(minParamIdx);
    cvBest.sparsityVal = sparsityVal(:,minParamIdx);
    %% get the best sparsity near the best mse
    nonActive = sparsityVal(nonIdx,:);
    avgNActNorm = sum(abs(nonActive));
    minNorm = min(avgNActNorm(abs(mseVal-cvBest.mseVal)<=cvBestSp.mseTol));
    minParamIdx = min(find(avgNActNorm==minNorm)); % extra min in case there are more with the same norm
    cvBestSp.a = a{minParamIdx}; cvBestSp.selected = selected{minParamIdx};
    cvBestSp.iter = size(convergenceHistory{minParamIdx},1);
    cvBestSp.elapsedTime = elapsedTime{minParamIdx};
    cvBestSp.minParamIdx = minParamIdx;
    cvBestSp.sparsityTrain = sparsityTrain{:,minParamIdx};
    cvBestSp.mseTrain = mseTrain(minParamIdx);
    cvBestSp.mseVal = mseVal(minParamIdx);
    cvBestSp.sparsityVal = sparsityVal(:,minParamIdx);
    % and save
    tic
    save(['../nls_data/expResults/',expCode,'/valid_',algo,'(',num2str(mu),')','_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'mseVal','sparsityVal','paramGrid','cvBest','cvBestSp')
    toc

    
  case 'LinGL' % liner GL algo as a baseline
    % get mse over test and the derivative norms
    fName1 = ['../nls_data/expData/',expCode,'/validData',num2str(trainLength),'_',num2str(rep),'.mat'];
    fName2 = ['../nls_data/expData/',expCode,'/validData','_',num2str(rep),'.mat'];
    fName3 = ['../nls_data/expData/',expCode,'/validData.mat'];
    if exist(fName1, 'file') == 2
      val = load(fName1,'x','y');
    elseif exist(fName2, 'file') == 2
      val = load(fName2,'x','y');
    else
      val = load(fName3,'x','y');
    end
%    val = load(['../nls_data/expData/',expCode,'/validData'],'x','y');
    load(['../nls_data/expResults/',expCode,'/model_',algo,'_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'a','selected','convergenceHistory','elapsedTime','tauGrid','mseTrain','sparsityTrain')
    sparsityVal = cell2mat(sparsityTrain); %(for consistency with nonlinear methods)
    errVals = bsxfun(@minus,val.x*cell2mat(a),val.y);
    mseVal = sum(errVals.^2)./size(val.x,1); % for each model in a column
    paramGrid = tauGrid;
    % find the best hyper-param values
    [~,minParamIdx] = min(mseVal);
    cvBest.a = a{minParamIdx}; cvBest.selected = selected{minParamIdx};
    cvBest.iter = size(convergenceHistory{minParamIdx},1);
    cvBest.elapsedTime = elapsedTime{minParamIdx};
    cvBest.minParamIdx = minParamIdx;
    cvBest.sparsityTrain = sparsityTrain{:,minParamIdx};
    cvBest.mseTrain = mseTrain(minParamIdx);
    cvBest.mseVal = mseVal(minParamIdx);
    cvBest.sparsityVal = sparsityVal(:,minParamIdx);
    %% get the best sparsity near the best mse
    nonActive = sparsityVal(nonIdx,:);
    avgNActNorm = sum(abs(nonActive));
    minNorm = min(avgNActNorm(abs(mseVal-cvBest.mseVal)<=cvBestSp.mseTol));
    minParamIdx = min(find(avgNActNorm==minNorm)); % extra min in case there are more with the same norm
    cvBestSp.a = a{minParamIdx}; cvBestSp.selected = selected{minParamIdx};
    cvBestSp.iter = size(convergenceHistory{minParamIdx},1);
    cvBestSp.elapsedTime = elapsedTime{minParamIdx};
    cvBestSp.minParamIdx = minParamIdx;
    cvBestSp.sparsityTrain = sparsityTrain{:,minParamIdx};
    cvBestSp.mseTrain = mseTrain(minParamIdx);
    cvBestSp.mseVal = mseVal(minParamIdx);
    cvBestSp.sparsityVal = sparsityVal(:,minParamIdx);
    % and save
    tic
    save(['../nls_data/expResults/',expCode,'/valid_',algo,'_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'mseVal','sparsityVal','paramGrid','cvBest','cvBestSp')
    toc

    
  case 'mean' % mean as baseline
    % get mse over test and the derivative norms
    fName1 = ['../nls_data/expData/',expCode,'/validData',num2str(trainLength),'_',num2str(rep),'.mat'];
    fName2 = ['../nls_data/expData/',expCode,'/validData','_',num2str(rep),'.mat'];
    fName3 = ['../nls_data/expData/',expCode,'/validData.mat'];
    if exist(fName1, 'file') == 2
      val = load(fName1,'x','y');
    elseif exist(fName2, 'file') == 2
      val = load(fName2,'x','y');
    else
      val = load(fName3,'x','y');
    end
%    val = load(['../nls_data/expData/',expCode,'/validData'],'x','y');
    load(['../nls_data/expResults/',expCode,'/model_',algo,'_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'a','elapsedTime','tauGrid','mseTrain','sparsityTrain')
    sparsityVal = sparsityTrain;
    errVals = bsxfun(@minus,repmat(cell2mat(a),size(val.y,1),1),val.y);
    mseVal = sum(errVals.^2)./size(val.x,1); % for each model in a column
    paramGrid = tauGrid;
    % find the best hyper-param values
    minParamIdx = 1; % they should be all the same so simply take the 1st one
    %[~,minParamIdx] = min(mseVal);
    cvBest.a = a{minParamIdx};
    cvBest.elapsedTime = elapsedTime{minParamIdx};
    cvBest.minParamIdx = minParamIdx;
    cvBest.sparsityTrain = sparsityTrain;
    cvBest.mseTrain = mseTrain(minParamIdx);
    cvBest.mseVal = mseVal(minParamIdx);
    cvBest.sparsityVal = sparsityVal(:,minParamIdx);
    %% get the best sparsity near the best mse
    nonActive = repmat(sparsityVal(nonIdx,:),1,length(mseVal)); % to get the right size for nonActive
    avgNActNorm = sum(abs(nonActive));
    minNorm = min(avgNActNorm(abs(mseVal-cvBest.mseVal)<=cvBestSp.mseTol));
    minParamIdx = min(find(avgNActNorm==minNorm)); % extra min in case there are more with the same norm
    cvBestSp.a = a{minParamIdx};
    cvBestSp.elapsedTime = elapsedTime{minParamIdx};
    cvBestSp.minParamIdx = minParamIdx;
    cvBestSp.sparsityTrain = sparsityTrain;
    cvBestSp.mseTrain = mseTrain(minParamIdx);
    cvBestSp.mseVal = mseVal(minParamIdx);
    cvBestSp.sparsityVal = sparsityVal(:,minParamIdx);
    % and save
    tic
    save(['../nls_data/expResults/',expCode,'/valid_',algo,'_',num2str(trainLength),'_',num2str(rep),'_lin1.mat'],'mseVal','sparsityVal','paramGrid','cvBest','cvBestSp')
    toc
    
  otherwise
    error('Unknown algo')
end





end





