function testModels(expCode,algo,kernelType,kernelParam,silent)
% TESTMODELS - test models for various algos
%
% INPUTS
%   expCode - code of the experiment
%   algo - code of the algo to use; for algos with mu include mu into the algo name in brackets after, e.g. denovas1(0)
%   kernelType - 'poly','polyHom' or 'Gauss'
%   kernelParam - degree of polynomials or sigma for Gauss
%   silent - if 1 will not printout progress
%
% EXAMPLE: testModels('exp001','L1','poly',2)
%
% CREATED: MG - 18/05/2017
% MG 11/09/2017: if validate is trainSample specific, load that instead of general single validate for all trainSamples

%% fill in optional parameters
if ~exist('silent','var') || isempty(silent),
  silent = 0;
end

%% correct folders 
[~,hn] = system('hostname');
switch hn(1:4)
  case 'node'
    cd /home/gregorom/nls ; % on baobab
  case 'magd'
    cd /home/magda/Bitbucket/nls; % for testing on local machine
  case 'unig'
    cd /home/magda/Bitbucket/nls; % for testing on local machine
  otherwise
    cd /user/ai2/gregoma0/nls; % on whales
end

%% get list of files
fList = dir(['../nls_data/expResults/',expCode,'/valid_',algo,'*']); fList = {fList.name}'; 
fNum = length(fList);
if ~(length(fList)>0)
  error('No cv results available for given input combinations')
end

%% from each file calculate the test results for best hyper-params
trainLengthVec = [];
for fIdx=1:fNum
  fName = fList{fIdx};
  % keep going only if results for the correct kernel
  if isempty(strfind(fName,[kernelType,num2str(kernelParam)]))
    continue
  end
  % get separators
  pos_ = strfind(fName,'_');
  if ~isempty(strfind(fName,'eNet')) % eNet has an extra underscore in the brackets so drop that
    pos_(1) = [];
  end
  trainLength = str2double(fName(pos_(2)+1:pos_(3)-1));
  rep = str2num(fName(pos_(3)+1:pos_(4)-1));
  % name for struct fields
  tL = ['tL_',num2str(trainLength)];
  % load model data
  load(['../nls_data/expResults/',expCode,'/',fName],'cvBest','cvBestSp');
  if isempty(strfind(fName,'Lin')) && isempty(strfind(fName,'mean')) % nonlinear models
    % get kernel matrices
    if ~silent
      fprintf('Getting kernel matrices tL=%s, rep=%d \n',tL,rep)
    end
    % get kernel matrices - do not need the special Denovas code here cause
    % once the parameters are calculated they are the same as our model
    % would give
    [K,Da,Da1,Lab,y] = getKernels(expCode,'test',trainLength,rep,kernelType,kernelParam);
    % precalculate summary matrices
    [nTr,n] = size(K); d = size(Da,3);
    DaTemp = permute(Da,[1,3,2]); D = reshape(DaTemp,[],n);
    LabTemp = permute(Lab,[1,3,2,4]); Lb = reshape(LabTemp,[],n,d);
    %% get mse over test and the derivative norms for crossvalidated best MSE
    if ~isfield(cvBest,'b') % for models that do not learn b
      cvBest.b = zeros(nTr*d,size(cvBest.a,2));
    end
    [mseTest(rep).(tL),sparsityTest(rep).(tL),stdErrorTest(rep).(tL)] = getMse(y,cvBest.a,cvBest.b,K,D,Da1,Lb); % watchout, need to pass in Da1 matrix to have the derivatives with respect to validation set
    %% get mse over test and the derivative norms for crossvalidated best sparsity near best MSE
    if ~isfield(cvBestSp,'b') % for models that do not learn b
      cvBestSp.b = zeros(nTr*d,size(cvBestSp.a,2));
    end
    [mseTestSp(rep).(tL),sparsityTestSp(rep).(tL),stdErrorTestSp(rep).(tL)] = getMse(y,cvBestSp.a,cvBestSp.b,K,D,Da1,Lb); % watchout, need to pass in Da1 matrix to have the derivatives with respect to validation set
  elseif isempty(strfind(fName,'mean')) % linear models
    %% get mse over test and the derivative norms for crossvalidated best MSE
    sparsityTest(rep).(tL) = cvBest.sparsityTrain;
    fName1 = ['../nls_data/expData/',expCode,'/testData',num2str(trainLength),'_',num2str(rep),'.mat'];
    fName2 = ['../nls_data/expData/',expCode,'/testData','_',num2str(rep),'.mat'];
    fName3 = ['../nls_data/expData/',expCode,'/testData.mat'];
    if exist(fName1, 'file') == 2
      tst = load(fName1,'x','y');
    elseif exist(fName2, 'file') == 2
      tst = load(fName2,'x','y');
    else
      tst = load(fName3,'x','y');
    end
%    tst = load(['../nls_data/expData/',expCode,'/testData'],'x','y');
    errVals = tst.y - tst.x*cvBest.a;
    mseTest(rep).(tL) = sum(errVals.^2)./size(tst.x,1); 
    stdErrorTest(rep).(tL) = std(errVals);
    %% get mse over test and the derivative norms for best sparsity near best MSE
    sparsityTestSp(rep).(tL) = cvBestSp.sparsityTrain;
    errVals = tst.y - tst.x*cvBestSp.a;
    mseTestSp(rep).(tL) = sum(errVals.^2)./size(tst.x,1); 
    stdErrorTestSp(rep).(tL) = std(errVals);
  else % mean model
    %% get mse over test and the derivative norms for crossvalidated best MSE
    sparsityTest(rep).(tL) = cvBest.sparsityTrain;
    fName1 = ['../nls_data/expData/',expCode,'/testData',num2str(trainLength),'_',num2str(rep),'.mat'];
    fName2 = ['../nls_data/expData/',expCode,'/testData','_',num2str(rep),'.mat'];
    fName3 = ['../nls_data/expData/',expCode,'/testData.mat'];
    if exist(fName1, 'file') == 2
      tst = load(fName1,'x','y');
    elseif exist(fName2, 'file') == 2
      tst = load(fName2,'x','y');
    else
      tst = load(fName3,'x','y');
    end
%    tst = load(['../nls_data/expData/',expCode,'/testData'],'x','y');
    errVals = tst.y - ones(length(tst.y),1)*cvBest.a;
    mseTest(rep).(tL) = sum(errVals.^2)./size(tst.x,1); 
    stdErrorTest(rep).(tL) = std(errVals);
    %% get mse over test and the derivative norms for best sparsity near best MSE
    sparsityTestSp(rep).(tL) = cvBestSp.sparsityTrain;
    errVals = tst.y - ones(length(tst.y),1)*cvBestSp.a;
    mseTestSp(rep).(tL) = sum(errVals.^2)./size(tst.x,1); 
    stdErrorTestSp(rep).(tL) = std(errVals);
  end
  mseTolSp(rep).(tL) = cvBestSp.mseTol;
end

%% save the summaries
save(['../nls_data/expResults/',expCode,'/testSummary_',algo,'_',kernelType,num2str(kernelParam),'.mat'],'mseTest','sparsityTest','stdErrorTest')
save(['../nls_data/expResults/',expCode,'/testSummary_',algo,'_',kernelType,num2str(kernelParam),'.mat'],'mseTestSp','sparsityTestSp','stdErrorTestSp','mseTolSp','-append')


% %% functions
% function [K,Da,Da1,Lab,y] = getKernels(expCode,'test',trainLength,rep,kernelType,kernelParam)
%   if exist(['../nls_data/expData/',expCode,'/testKernels',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'], 'file') == 2
%     % load kernel matrices
%     load(['../nls_data/expData/',expCode,'/testKernels',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'])
%     % load output data 
%     load(['../nls_data/expData/',expCode,'/testData'],'y')
%   else
%     % load input and output data
%     load(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep)],'x')
%     xTrain = x;
%     load(['../nls_data/expData/',expCode,'/testData'],'x','y')  
%     % get kernel matrices
%     [K,Da,Da1,Lab] = getKernelMatrices(xTrain,x,kernelType,kernelParam); 
%     save(['../nls_data/expData/',expCode,'/testKernels',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'K','Da','Da1','Lab')
%   end
% end


end





