function genHouseData
% GENHOUSEDATA - generate experimenatl data from Kaggle House Sales in King County, USA dataset
%
% CREATED: MG 26/09/2017

%% create data directories
%%%%%%%%%%%%%%%%%%%%%%%%%%
mkdir(['../nls_data/expData/','house'])
% also for the results and prints
mkdir(['../nls_data/expResults/','house'])
mkdir(['../nls_data/expPrints/','house'])

%% load kc_house_data
load('/home/magda/Dropbox/Data/Kaggle/House/kc_house_data.mat','house_data_short_Array','colNames');
[n,d] = size(house_data_short_Array);

%% prepare test and validation samples
rng(n)
for rep=1:20;
% training data
  for trainLength=[50:50:200]; % for various lengths
    while 1
      trainData = datasample(house_data_short_Array,trainLength);
      if all(sum(trainData)) % make sure all samples have at least 1 positive for all categorical variables
        break
      end
    end
    trMean = mean(trainData);
    trStd = std(trainData);
    trainData = bsxfun(@minus,trainData,trMean);
    trainData = bsxfun(@rdivide,trainData,trStd); 
    y = trainData(:,1);
    x = trainData(:,2:end);
    save(['../nls_data/expData/house/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y')
    % validation sample
    validData = datasample(house_data_short_Array,1000);
    validData = bsxfun(@minus,validData,trMean);
    validData = bsxfun(@rdivide,validData,trStd);
    validData(isnan(validData)) = 0; % correct the nans
    y = validData(:,1);
    x = validData(:,2:end);
    save(['../nls_data/expData/house/validData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y')
    % test sample
    testData = datasample(house_data_short_Array,1000);
    testData = bsxfun(@minus,testData,trMean);
    testData = bsxfun(@rdivide,testData,trStd);
    testData(isnan(testData)) = 0; % correct the nans
    y = testData(:,1);
    x = testData(:,2:end);
    save(['../nls_data/expData/house/testData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y')
  end
end


end