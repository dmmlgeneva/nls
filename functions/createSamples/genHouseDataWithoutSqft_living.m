function genHouseDataWithoutSqft_living
% GENHOUSEDATAWITHOUTSQFT_LIVING - generate experimenatl data from Kaggle House Sales in King County, USA dataset
% drop the sqft_living which is the most important predictor
%
% CREATED: MG 24/09/2017

%% create data directories
%%%%%%%%%%%%%%%%%%%%%%%%%%
mkdir(['../nls_data/expData/','houseW'])
% also for the results and prints
mkdir(['../nls_data/expResults/','houseW'])
mkdir(['../nls_data/expPrints/','houseW'])

%% load kc_house_data
load('/home/magda/Dropbox/Data/Kaggle/kc_house_data.mat','house_data_short_Array','colNames');
[n,d] = size(house_data_short_Array);

%% standardize the x coordinates (do it for the whole dataset for simplicity)
house_data_short_Array(:,2) = [];
house_data_short_Array(:,2:end)= bsxfun(@minus, house_data_short_Array(:,2:end), mean(house_data_short_Array(:,2:end)));
house_data_short_Array(:,2:end)= bsxfun(@rdivide, house_data_short_Array(:,2:end), std(house_data_short_Array(:,2:end)));

%% prepare test and validation samples
rng(n)
for rep=1:5;
% training data
  for trainLength=[100:50:300]; % for various lengths
    tempData = datasample(house_data_short_Array,trainLength);
    y = tempData(:,1);
    x = tempData(:,2:end);
    save(['../nls_data/expData/houseW/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y')
  end
end
% long validation data - some may be the same as in the training but this is ok, standard sampling with replacement
tempData = datasample(house_data_short_Array,1000);
y = tempData(:,1);
x = tempData(:,2:end);
save(['../nls_data/expData/houseW/validData.mat'],'x','y')
% long test data - some may be the same as in the training or validation but this is ok, standard sampling with replacement
tempData = datasample(house_data_short_Array,1000);
y = tempData(:,1);
x = tempData(:,2:end);
save(['../nls_data/expData/houseW/testData.mat'],'x','y')

end