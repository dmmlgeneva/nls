function genRosascoData()
% GENROSASODATA - generate data as in section 6.2 of Rosasco et al:Nonparametric sparsity and Regularization (2013)
%
% CREATED: MG - 19/4/2017

%% Experiment Ros1: Adaption to marginal distribution

mkdir('expData/Ros1')

function [x,w,y]=genData1(n)
% x1 ~ uniform(-1,1)
x1 = -1 + 2.*rand(n,1);
% x2 ~ N(0,0.05)
x2 = normrnd(0,0.05,n,1);
x=[x1 x2];
% noise w ~ N(0,0.1)
w = normrnd(0,0.1,n,1);
y = x1.^2 + w;
end

trainLength=20;
validLength=20;

[x,w,y]=genData1(trainLength);
save(['expData/Ros1/trainData.mat'],'x','w','y')
[x,w,y]=genData1(validLength);
save(['expData/Ros1/validData.mat'],'x','w','y')

%% Experiment Ros2: Effect of varying v

mkdir('expData/Ros2')

function [x,w,y]=genData2(n)
% x ~ uniform(-1,1)^20
x = -1 + 2.*rand(n,20);
% noise w ~ N(0,1)
w = normrnd(0,1,n,1);
y = zeros(n,1);
for a=1:4
  for b=a+1:4
    y = y + x(:,a).*x(:,b);
  end
end
% lbda - to keep signal to noise ration to 15:1 
% variance of y is approx 7/9
lbda = sqrt(15*9/6);
%var(lbda*y)
y = lbda*y + w;
end

for rep=1:20

  for trainLength=[50:10:120];
    [x,w,y]=genData2(trainLength);
    save(['expData/Ros2/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','w','y')
  end
  for validLength=[50:10:120];
    [x,w,y]=genData2(validLength);
    save(['expData/Ros2/validData',num2str(validLength),'_',num2str(rep),'.mat'],'x','w','y')
  end
  testLength=500;
  [x,w,y]=genData2(testLength);
  save(['expData/Ros2/testData','_',num2str(rep),'.mat'],'x','w','y')

end


end