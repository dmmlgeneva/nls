function genExpData(expCode)
% GENEXPDATA - generate experimenatl data
%
% INPUTS
%   expCode - code of the experiment
%
% EXAMPLE: genExpData('exp001')
%
% CREATED: MG - 12/05/2017

%% create data directories
%%%%%%%%%%%%%%%%%%%%%%%%%%
mkdir(['../nls_data/expData/',expCode])
% also for the results and prints
mkdir(['../nls_data/expResults/',expCode])
mkdir(['../nls_data/expPrints/',expCode])



%% choose experiment function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch expCode
  case 'exp001'
    % training data
    for rep=1:20 % 20 replications
      for trainLength=[50:20:150]; % for various lengths
        [x,y]=genDataExp001(trainLength);
        save(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y')
      end
    end
    % long validation data
    [x,y]=genDataExp001(1000);
    save(['../nls_data/expData/',expCode,'/validData.mat'],'x','y')
    % long test data
    [x,y]=genDataExp001(1000);
    save(['../nls_data/expData/',expCode,'/testData.mat'],'x','y')

  case 'exp002'
    % training data
    for rep=1:20 % 20 replications
      for trainLength=[50:20:150]; % for various lengths
        [x,y,w]=genDataExp002(trainLength);
        save(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y','w')
      end
    end
    % long validation data
    [x,y,w]=genDataExp002(1000);
    save(['../nls_data/expData/',expCode,'/validData.mat'],'x','y','w')
    % long test data
    [x,y,w]=genDataExp002(1000);
    save(['../nls_data/expData/',expCode,'/testData.mat'],'x','y','w')
    
  case 'exp003'
    % training data
    for rep=1:20 % 20 replications
      for trainLength=[50:20:150]; % for various lengths
        [x,y,w]=genDataExp003(trainLength);
        save(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y','w')
      end
    end
    % long validation data
    [x,y,w]=genDataExp003(1000);
    save(['../nls_data/expData/',expCode,'/validData.mat'],'x','y','w')
    % long test data
    [x,y,w]=genDataExp003(1000);
    save(['../nls_data/expData/',expCode,'/testData.mat'],'x','y','w')

  case 'exp004'
    % training data
    coefs = -1 + 2.*rand(6^3,1);
    for rep=1:20 % 20 replications
      for trainLength=[50:20:150]; % for various lengths
        grps = -1 + 2.*rand(trainLength,4);
        [x,y]=genDataExp004(trainLength,grps,coefs);
        save(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y','grps','coefs')
      end
    end
    % long validation data
    grps = -1 + 2.*rand(1000,4);
    [x,y]=genDataExp004(1000,grps,coefs);
    save(['../nls_data/expData/',expCode,'/validData.mat'],'x','y','grps','coefs')
    % long test data
    grps = -1 + 2.*rand(1000,4);
    [x,y]=genDataExp004(1000,grps,coefs);
    save(['../nls_data/expData/',expCode,'/testData.mat'],'x','y','grps','coefs')

  case 'exp005'
    % training data
    for rep=1:20 % 20 replications
      for trainLength=[50:20:150]; % for various lengths
        grps = -1 + 2.*rand(trainLength,4);
        [x,y,w]=genDataExp005(trainLength,grps);
        save(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y','grps','w')
      end
    end
    % long validation data
    grps = -1 + 2.*rand(1000,4);
    [x,y,w]=genDataExp005(1000,grps);
    save(['../nls_data/expData/',expCode,'/validData.mat'],'x','y','grps','w')
    % long test data
    grps = -1 + 2.*rand(1000,4);
    [x,y,w]=genDataExp005(1000,grps);
    save(['../nls_data/expData/',expCode,'/testData.mat'],'x','y','grps','w')
    

  case 'exp006' % favourable for elastic net - hmmm, didn treally work
    % training data
    coefs = [0.8 0.05 -0.5 0.9 0.03 -0.1];
    for rep=1:20 % 20 replications
      for trainLength=[50:20:150]; % for various lengths
        [x,y,w]=genDataExp006(trainLength,coefs);
        save(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y','coefs','w')
      end
    end
    % long validation data
    [x,y,w]=genDataExp006(1000,coefs);
    save(['../nls_data/expData/',expCode,'/validData.mat'],'x','y','w','coefs')
    % long test data
    [x,y,w]=genDataExp006(1000,coefs);
    save(['../nls_data/expData/',expCode,'/testData.mat'],'x','y','w','coefs')
    

  case 'exp007' % favourable for elastic net - they are all too good
    % training data
    for rep=1:20 % 20 replications
      for trainLength=[50:20:150]; % for various lengths
        grps = -1 + 2.*rand(trainLength,5);
        [x,y,w]=genDataExp007(trainLength,grps);
        save(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y','grps','w')
      end
    end
    % long validation data
    grps = -1 + 2.*rand(1000,5);
    [x,y,w]=genDataExp007(1000,grps);
    save(['../nls_data/expData/',expCode,'/validData.mat'],'x','y','grps','w')
    % long test data
    grps = -1 + 2.*rand(1000,5);
    [x,y,w]=genDataExp007(1000,grps);
    save(['../nls_data/expData/',expCode,'/testData.mat'],'x','y','grps','w')
    

  case 'exp008' % favourable for elastic net - same as 007 but larger noise
    % training data
    for rep=1:20 % 20 replications
      for trainLength=[50:20:150]; % for various lengths
        grps = -1 + 2.*rand(trainLength,5);
        [x,y,w]=genDataExp008(trainLength,grps);
        save(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y','grps','w')
      end
    end
    % long validation data
    grps = -1 + 2.*rand(1000,5);
    [x,y,w]=genDataExp008(1000,grps);
    save(['../nls_data/expData/',expCode,'/validData.mat'],'x','y','grps','w')
    % long test data
    grps = -1 + 2.*rand(1000,5);
    [x,y,w]=genDataExp008(1000,grps);
    save(['../nls_data/expData/',expCode,'/testData.mat'],'x','y','grps','w')
    
    
    
  otherwise
    error('Wrong experiment code')
end

%% define functions to generate the data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% noiseless 2nd order polynomial from 1st 4 features out of 20
function [x,y]=genDataExp001(n)
  x = -1 + 2.*rand(n,20);
  y = zeros(n,1);
  for a=1:4
    for b=1:4
      y = y + x(:,a).*x(:,b);
    end
  end
end

% noisy 2nd order polynomial from 1st 4 features out of 20
function [x,y,w]=genDataExp002(n)
  x = -1 + 2.*rand(n,20);
  y = zeros(n,1);
  for a=1:4
    for b=1:4
      y = y + x(:,a).*x(:,b);
    end
  end
w = normrnd(0,1,n,1);
y = y + w;
end

% noisy (smaller noise than exp002) 2nd order polynomial from 1st 4 features out of 20
function [x,y,w]=genDataExp003(n)
  x = -1 + 2.*rand(n,20);
  y = zeros(n,1);
  for a=1:4
    for b=1:4
      y = y + x(:,a).*x(:,b);
    end
  end
w = normrnd(0,0.01,n,1);
y = y + w;
end

% noiseless 3rd order polynomial from 1st 6 features out of 12 with
% correlated groups 1-3,4-6,7-9,10-12
function [x,y]=genDataExp004(n,grps,coefs)
  grps = kron(grps,ones(1,3));
  for i = 1:12
    x(:,i) = grps(:,i) + normrnd(0,0.2,n,1);
  end
  y = zeros(n,1);
  idx = 0;
  for a=1:6
    for b=1:6
      for c=1:6
        idx = idx+1;
        y = y + coefs(idx)*x(:,a).*x(:,b).*x(:,c);
      end
    end
  end
end


% noisy (smaller noise than exp002) 3rd order polynomial from 1st 6 features out of 12
% correlated groups 1-3,4-6,7-9,10-12
function [x,y,w]=genDataExp005(n,grps)
  grps = kron(grps,ones(1,3));
  for i = 1:12
    x(:,i) = grps(:,i) + normrnd(0,0.2,n,1);
  end
  y = zeros(n,1);
  for a=1:6
    for b=1:6
      for c=1:6
        y = y + x(:,a).*x(:,b).*x(:,c);
      end
    end
  end
w = normrnd(0,0.01,n,1);
y = y + w;
end


% 2nd order polynomials
function [x,y,w]=genDataExp006(n,coefs)
  x = -1 + 2.*rand(n,12);
  y =  coefs(1)*x(:,1).*x(:,4) + coefs(2)*x(:,2).*x(:,4) + coefs(3)*x(:,3).*x(:,4) + ...
    coefs(4)*x(:,5).*x(:,8) + coefs(5)*x(:,6).*x(:,8) + coefs(6)*x(:,7).*x(:,8);
  w = normrnd(0,0.01,n,1);
  y = y + w;
end

% noisy (smaller noise than exp002) 2nd order polynomial from 1st 6 features out of 12
% correlated groups 1-3,4-6,7-9,10-12
function [x,y,w]=genDataExp007(n,grps)
  grps = [grps(:,1) grps(:,1) grps(:,2) grps(:,3) grps(:,3) grps(:,4) repmat(grps(:,5),1,6)];
  for i = 1:12
    x(:,i) = grps(:,i) + normrnd(0,0.1,n,1);
  end
  y = x(:,1).*x(:,3) + x(:,2).*x(:,3) + x(:,4).*x(:,6) + x(:,5).*x(:,6);
  w = normrnd(0,0.01,n,1);
  y = y + w;
end

% same as 7 but larger noise
function [x,y,w]=genDataExp008(n,grps)
  grps = [grps(:,1) grps(:,1) grps(:,2) grps(:,3) grps(:,3) grps(:,4) repmat(grps(:,5),1,6)];
  for i = 1:12
    x(:,i) = grps(:,i) + normrnd(0,0.1,n,1);
  end
  y = x(:,1).*x(:,3) + x(:,2).*x(:,3) + x(:,4).*x(:,6) + x(:,5).*x(:,6);
  w = normrnd(0,0.1,n,1);
  y = y + w;
end

end
