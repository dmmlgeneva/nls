function genHouseDataCorrupt
% GENHOUSEDATASHUFFLE - generate experimenatl data from Kaggle House Sales in King County, USA dataset
% the 2nd half of features is the same as 1st part but shuffled so that not predictive
%
% CREATED: MG 5/10/2017

%% create data directories
%%%%%%%%%%%%%%%%%%%%%%%%%%
expName = 'CorHouse';
mkdir(['../nls_data/expData/',expName])
% also for the results and prints
mkdir(['../nls_data/expResults/',expName])
mkdir(['../nls_data/expPrints/',expName])

%% load kc_house_data
load('/home/magda/Dropbox/Data/Kaggle/House/kc_house_data_conti.mat','house_data_short_Array','colNames');
[n,d] = size(house_data_short_Array);

%% replicate the features and shuffle them
dataCombi = house_data_short_Array(:,4:10);
feats = dataCombi + normrnd(0,1,n,size(dataCombi,2));
dataCombi = [house_data_short_Array(:,1) dataCombi feats];

%% prepare test and validation samples
rng(n)
for rep=1:20;
% training data
  for trainLength=[50:50:200]; % for various lengths
    while 1
      trainData = datasample(dataCombi,trainLength);
      if all(sum(trainData)) % make sure all samples have at least 1 positive for all variables
        break
      end
    end
    trMean = mean(trainData);
    trStd = std(trainData);
    trainData = bsxfun(@minus,trainData,trMean);
    trainData = bsxfun(@rdivide,trainData,trStd); 
    y = trainData(:,1);
    x = trainData(:,2:end);
    save(['../nls_data/expData/',expName,'/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y')
    % validation sample
    validData = datasample(dataCombi,1000);
    validData = bsxfun(@minus,validData,trMean);
    validData = bsxfun(@rdivide,validData,trStd);
    validData(isnan(validData)) = 0; % correct the nans
    y = validData(:,1);
    x = validData(:,2:end);
    save(['../nls_data/expData/',expName,'/validData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y')
    % test sample
    testData = datasample(dataCombi,1000);
    testData = bsxfun(@minus,testData,trMean);
    testData = bsxfun(@rdivide,testData,trStd);
    testData(isnan(testData)) = 0; % correct the nans
    y = testData(:,1);
    x = testData(:,2:end);
    save(['../nls_data/expData/',expName,'/testData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y')
  end
end


end