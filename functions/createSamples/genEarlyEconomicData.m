function genEarlyEconomicData
% GENEARLYECONOMICDATA - generate experimenatl data from Stock&Watson 2012 economic data 
% based on genEconomicData but using larger dataset with more input variables
%
% CREATED: MG 26/09/2017

%% load SW data
load('../nls_data/SW.mat','midData','midNames');

%% keep only CEE dataset of Banbura, M., Giannone, D., & Reichlin, L. (2010). Large Bayesian vector auto regressions. 
% employees on non-farm payrolls CES002
% consumer price index CPIAUCSL
% federal fund rate FYFF
% index of sensitive material prices PSCCOMR
% non-borrower reserves FMRNBA
% totatal reserves FMRRA
% M2 money stock FM2
selIdx = ismember(midNames,{'CES002','CPIAUCSL','FYFF','PSCCOMR','FMRNBA','FMRRA','FM2'});
ecoNames = midNames(selIdx);
ecoData = midData(:,selIdx);


%% create data directories
for yIdx = 1:3
  mkdir(['../nls_data/expData/','Eec00',num2str(yIdx)])
  % also for the results and prints
  mkdir(['../nls_data/expResults/','Eec00',num2str(yIdx)])
  mkdir(['../nls_data/expPrints/','Eec00',num2str(yIdx)])
end


%% create trian and test datasets
numLagsIn = 4; % number of lags
numReplications = 30;
for trainLength = 40:40:160 % number of training points

  for rep = 1:numReplications
    replData = ecoData(1+rep-1:trainLength+numLagsIn+rep,:);
    % remove mean and standardize
    replData = bsxfun(@minus, replData, mean(replData));
    replData = bsxfun(@rdivide, replData, std(replData));

    %% create the inputs and outputs (brute force, no time to search for elegant solutions
    [T,m] = size(replData);
    inputs = [];
    for tsIdx = 1:m
      for lagIdx = 1:numLagsIn
        inputs = [inputs replData((1+numLagsIn-lagIdx):end-lagIdx,tsIdx)]; 
      end
    end
    outputs = [replData((1+numLagsIn):end,:)];

    %% split test, validation and train dataset (test only has the last datapoint) and save
    % test set
    x = inputs(end,:);
    for yIdx=1:3
      y = outputs(end,yIdx);
      expNameTemp = ['Eec00',num2str(yIdx)];
      save(['../nls_data/expData/',expNameTemp,'/testData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y')
    end
    % drop the last observation from inputs and outputs for creating the validation and train samples
    inputs(end,:) = [];
    outputs(end,:) = [];
    % validation indexes
    validIdx = false(trainLength,1);
    validIdx(1:round(0.3*trainLength)) = true; % use 1/3rd of data for validation
    validIdx = validIdx(randperm(trainLength));
    % validation set
    x = inputs(validIdx,:);
    for yIdx=1:3
      y = outputs(validIdx,yIdx);
      expNameTemp = ['Eec00',num2str(yIdx)];
      save(['../nls_data/expData/',expNameTemp,'/validData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y')
    end
    % train set
    x = inputs(~validIdx,:);
    for yIdx=1:3
      y = outputs(~validIdx,yIdx);
      expNameTemp = ['Eec00',num2str(yIdx)];
      save(['../nls_data/expData/',expNameTemp,'/trainData',num2str(trainLength),'_',num2str(rep),'.mat'],'x','y')
    end

  end
end





end