function [K,Da,Da1,Lab] = getKernelMatrices(xTrain,xTest,kernelType,kernelParam)
% GETKERNELMATRICES - get kernal gram matrix and the kernel derivative matrices across train and test (or validation) instances
%   note: does not rescale the kernel matrices anyhow
%
% INPUTS
%   xTrain - n1 x d matrix with train input features
%   xTest - n2 x d matrix with test (validation) input features; pass in the train data agin, if shall be just for training
%   kernelType - type of the kernel (1 - polynomial)
%   kernelParam - kernel parameter (specific for the type)
%
% OUTPUTS
%   K - n1 x n2 gram matrix with elements K(i,j) = kFun(xi,xj)
%   Da - n1 x n2 x d matrix of kernel derivatives with elements Da(i,j,a) = partial(kFun(xi,xj))/partial(xi_a)
%   Da1 - n2 x n1 x d matrix same as Da but xi is xTest and xj is xTrain
%   Lab - n1 x n2 x d x d matrix of kernel 2nd derivatives with elements Lab(i,j,a,b) = partial^2(kFun(xi,xj))/partial(xi_a)partial(xj_b)
%     
% EXAMPLE: K = getKernel(xTrain,xTest,'poly',2)
%
% CREATED: MG - 14/05/2017
% MG 13/09/2017 - make all kernel matrices single precison for memory and speed

%% choose the kernel function
switch kernelType
  case 'polyHom'
    kFunVec = @(xTr,xTe) polyHomogenousKernel(xTr,xTe,kernelParam);
  case 'poly'
    kFunVec = @(xTr,xTe) polyKernel(xTr,xTe,kernelParam);
  case 'gauss'
    kFunVec = @(xTr,xTe) gaussKernel(xTr,xTe,kernelParam);
  case 'lin'
    kFunVec = @(xTr,xTe) linKernel(xTr,xTe,kernelParam);
  otherwise
    error('unknown kernel')
end

%% get the matrices
if nargout==1
  [K] = kFunVec(xTrain,xTest);
%   K = single(K);
elseif nargout==2
  [K,Da] = kFunVec(xTrain,xTest);
%   K = single(K);
%   Da = single(Da);
elseif nargout==3
  [K,Da,Da1] = kFunVec(xTrain,xTest);
%   K = single(K);
%   Da = single(Da);
%   Da1 = single(Da1);
else
  [K,Da,Da1,Lab] = kFunVec(xTrain,xTest);
%   K = single(K);
%   Da = single(Da);
%   Da1 = single(Da1);
%   Lab = single(Lab);
end

%% define the kernel functions
function [k,da,da1,lab] = polyHomogenousKernel(xTr,xTe,p)
  d = size(xTe,2);
  linK = (xTr*xTe');
  k = linK.^p;
  if nargout>1
    dTemp = p*linK.^(p-1);
    dTemp1 = p*(xTe*xTr').^(p-1);
    for a=1:d
      da(:,:,a) = bsxfun(@times,dTemp,[xTe(:,a)]');
      if nargout > 2
        da1(:,:,a) = bsxfun(@times,dTemp1,[xTr(:,a)]');
        if nargout > 2
          laTemp(:,:,a) = bsxfun(@times,p*(p-1)*linK.^(p-2),[xTe(:,a)]');
          for b=1:d
            lab(:,:,a,b) = bsxfun(@times,laTemp(:,:,a),xTr(:,b)) + logical(a==b)*dTemp;
          end
        end
      end
    end
  end
end


function [k,da,da1,lab] = polyKernel(xTr,xTe,p)
  d = size(xTe,2);
  linK = (xTr*xTe'+1);
  k = linK.^p;
  if nargout > 1
    dTemp = p*linK.^(p-1);
    dTemp1 = p*(xTe*xTr'+1).^(p-1);
    for a=1:d
      da(:,:,a) = bsxfun(@times,dTemp,[xTe(:,a)]');
      if nargout > 2
        da1(:,:,a) = bsxfun(@times,dTemp1,[xTr(:,a)]');
        if nargout > 3
          laTemp(:,:,a) = bsxfun(@times,p*(p-1)*linK.^(p-2),[xTe(:,a)]');
          for b=1:d
            lab(:,:,a,b) = bsxfun(@times,laTemp(:,:,a),xTr(:,b)) + logical(a==b)*dTemp;
          end
        end
      end
    end 
  end
end

%% eq 1.27-1.29 in FutureWork2017March
function [k,da,da1,lab] = gaussKernel(xTr,xTe,p)
  [n,d] = size(xTr);
  n1 = size(xTe,1);
  rowsqTr = sum(xTr.^2,2);
  rowsqTe = sum(xTe.^2,2);
  prodX = 2*xTr*xTe';
  KTemp = bsxfun(@minus,rowsqTr,prodX);
  KTemp = bsxfun(@plus,KTemp,rowsqTe');
  k = exp(-KTemp/ (2*p^2));

  da = zeros(n,n1,d);
  da1 = zeros(n1,n,d);
  lab = zeros(n,n1,d,d);

  if nargout>1
    for a=1:d
      dTemp = bsxfun(@plus,-xTr(:,a),xTe(:,a)');
      da(:,:,a) = k.*dTemp/ (p^2);
      da1(:,:,a) = k'.*(-dTemp')/ (p^2);
      if nargout > 3
        for b=1:d
          lTemp = bsxfun(@plus,xTr(:,b),-xTe(:,b)');
          lab(:,:,a,b) = da(:,:,a).*lTemp / (p^2) + logical(a==b)*k/(p^2);
        end
      end
    end
  end
end

%% define the kernel functions
function [k,da,da1,lab] = linKernel(xTr,xTe,p)
  d = size(xTe,2);
  k = (xTr*xTe');
  if nargout>1
    for a=1:d
      da(:,:,a) = repmat([xTe(:,a)]',size(xTr,1),1);
      if nargout > 2
        da1(:,:,a) = repmat([xTr(:,a)]',size(xTe,1),1);
        if nargout > 2
          for b=1:d
            lab(:,:,a,b) = zeros(size(k)) + logical(a==b)*1;
          end
        end
      end
    end
  end
end


%% OBSOLETE - slower naive version
% % define the kernel and the derivative functions
% switch kernelType
%   case 'polyHom'
%     p = kernelParam; % polynomial degree
%     kFun = @(xi,xj) dot(xi,xj)^p;
%     dFun = @(xi,xj,a) p*dot(xi,xj)^(p-1)*xj(a);
%     lFun = @(xi,xj,a,b) logical(a~=b)*p*(p-1)*dot(xi,xj)^(p-2)*xi(b)*xj(a) ...
%       + logical(a==b)*p*(p-1)*dot(xi,xj)^(p-2)*(xi(a)*xj(a) + dot(xi,xj)/(p-1));
%   otherwise
%     error('unknown kernel')
% end
% 
% % get the K,Da,Lab matrices
% [n1,d] = size(xTrain);
% n2 = size(xTest,1);
% K = zeros(n1,n2);
% Da = zeros(n1,n2,d);
% Lab = zeros(n1,n2,d,d);
% tic
% for i=1:n1
%   for j=1:n2 % will be symmetrical so only need upper triangular
%     K(i,j) = kFun(xTrain(i,:),xTest(j,:));
%      for a=1:d
%        Da(i,j,a) = dFun(xTrain(i,:),xTest(j,:),a);
%        for b=1:d
%          Lab(i,j,a,b) = lFun(xTrain(i,:),xTest(j,:),a,b);
%        end
%      end
%   end
% end
% toc

% %% This is most likely very stupid way of doing this but ok for now (eq 1.27-1.29 in FutureWork2017March
% function [k,da,da1,lab] = gaussKernel(xTr,xTe,p)
%   [n,d] = size(xTr);
%   n1 = size(xTe,1);
%   k = zeros(n,n1);
%   da = zeros(n,n1,d);
%   da1 = zeros(n1,n,d);
%   lab = zeros(n,n1,d,d);
%   for i = 1:n
%     for j = 1:n1
%       k(i,j) = exp( - norm(xTr(i,:)-xTe(j,:))^2 / (2*p^2) );
%       for a = 1:d
%         da(i,j,a) = k(i,j)*(xTe(j,a)-xTr(i,a))/ (p^2);
%         for b = 1:d
%           if a==b
%             lab(i,j,a,b) = -k(i,j)*( (xTr(i,a)-xTe(j,a))^2/(p^4) - 1/(p^2) );
%           else
%             lab(i,j,a,b) = k(i,j)*(xTe(j,a)-xTr(i,a))*(xTr(i,b)-xTe(j,b))/ (p^4);
%           end
%         end
%       end    
%     end
%   end
%   xTr1=xTe; xTe1 = xTr;
%   for i = 1:n1
%     for j = 1:n
%       k1(i,j) = exp( - norm(xTr1(i,:)-xTe1(j,:))^2 / (2*p^2) );
%       for a = 1:d
%         da1(i,j,a) = k1(i,j)*(xTe1(j,a)-xTr1(i,a))/ (p^2);
%       end    
%     end
%   end
% end



end