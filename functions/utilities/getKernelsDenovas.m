function [K,Z,L,y] = getKernelsDenovas(expCode,trainLength,rep,kernelType,kernelParam)
% GETKERNELSDENOVAS - get gram matrices for denovas; either load or calculate them
%
% INPUTS
%   expCode - code of the experiment
%   trainLength - length of train data
%   rep - data replication number
%   kernelType - 'poly','polyHom' or 'Gauss'
%   kernelParam - degree of polynomials or sigma for Gauss
%
% OUTPUTS
%   K,D,Da1,La - derivative and kernel matrices
%
% CREATED: MG - 06/09/2017
% MG 07/09/2017: drop saving of the kernel, they take too much space and are calculated fairly quickly

    % load input and output data
    load(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep)],'x','y')
    % get kernel matrices
    [K,Z,L] = nvs_kernel_matrices(x,kernelType,kernelParam); 

%   if exist(['../nls_data/expData/',expCode,'/trainKernelsDenovas',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'], 'file') == 2
%     % load kernel matrices
%     load(['../nls_data/expData/',expCode,'/trainKernelsDenovas',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'])
%     % load output data 
%     load(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep)],'y')
%   else
%     % load input and output data
%     load(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep)],'x','y')
%     % get kernel matrices
%     [K,Z,L] = nvs_kernel_matrices(x,kernelType,kernelParam); 
%     save(['../nls_data/expData/',expCode,'/trainKernelsDenovas',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'K','Z','L')
%   end
end