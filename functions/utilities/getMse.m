function [mse,sparsity,sdev] = getMse(y,aMat,bMat,K,D,Da,La)
% GETMSE - get mse for all sets of parameters in aMat and bMat
%
% INPUTS
%   y - true outputs
%   aMat, bMat - n x m, nd x m matrices of model parameters, each column is a different model
%   K,D,Da1,La - derivative and kernel matrices
%
% OUTPUTS
%   mse - 1 x m vector of average mse per model
%     
% EXAMPLE: K = getKernel(xTrain,xTest,'poly',2)
%
% CREATED: MG - 14/05/2017

% get mse
n = size(K,2);
funcVals = K'*aMat + D'*bMat; % for each model in column
errVals = bsxfun(@minus,funcVals,y);
mse = sum(errVals.^2)./n; % mse for each model in a column
sdev = std(errVals); % std for each model in a column

% get sparsity
if nargout>1
  m = size(aMat,2);
  d = size(bMat,1)/size(aMat,1);
  for mIdx = 1:m
    for i = 1:d
      derVals = Da(:,:,i)*aMat(:,mIdx) + [La(:,:,i)]'*bMat(:,mIdx); % derivative values for each dimension
      sparsity(i,mIdx) = sqrt(sum(derVals.^2))/sqrt(n); % average norm of the derivatives across instances for each dimension
    end
  end
end

end
