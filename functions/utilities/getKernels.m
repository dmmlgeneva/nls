function [K,Da,Da1,Lab,y] = getKernels(expCode,dataType,trainLength,rep,kernelType,kernelParam)
% GETKERNELS - get gram matrices for our method; either load or calculate them
%
% INPUTS
%   expCode - code of the experiment
%   dataType - train, valid or test
%   trainLength - length of train data
%   rep - data replication number
%   kernelType - 'poly','polyHom' or 'Gauss'
%   kernelParam - degree of polynomials or sigma for Gauss
%
% OUTPUTS
%   K,D,Da1,La - derivative and kernel matrices
%
% CREATED: MG - 06/09/2017
% MG 07/09/2017: drop saving of the kernel, they take too much space and are calculated fairly quickly
% MG 11/09/2017: if validate and test are trainSample specific, load those instead of general single validate and test for all trainSamples

    % load input and output data
    load(['../nls_data/expData/',expCode,'/trainData',num2str(trainLength),'_',num2str(rep)],'x','y')
    xTrain = x;
    if isempty(strfind(dataType,'train')) % load valid or test data if this is not for train sample
      fName1 = ['../nls_data/expData/',expCode,'/',dataType,'Data',num2str(trainLength),'_',num2str(rep),'.mat'];
      fName2 = ['../nls_data/expData/',expCode,'/',dataType,'Data','_',num2str(rep),'.mat'];
      fName3 = ['../nls_data/expData/',expCode,'/',dataType,'Data.mat'];
      if exist(fName1, 'file') == 2
        load(fName1,'x','y');
      elseif exist(fName2, 'file') == 2
        load(fName2,'x','y');
      else
        load(fName3,'x','y');
      end
    end
    % get kernel matrices
    [K,Da,Da1,Lab] = getKernelMatrices(xTrain,x,kernelType,kernelParam); 

%   if exist(['../nls_data/expData/',expCode,'/',dataType,'Kernels',num2str(dataLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'], 'file') == 2
%     % load kernel matrices
%     load(['../nls_data/expData/',expCode,'/',dataType,'Kernels',num2str(dataLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'])
%     % load output data 
%     load(['../nls_data/expData/',expCode,'/',dataType,'Data',num2str(dataLength),'_',num2str(rep)],'y')
%   else
%     % load input and output data
%     load(['../nls_data/expData/',expCode,'/trainData',num2str(dataLength),'_',num2str(rep)],'x')
%     xTrain = x;
%     load(['../nls_data/expData/',expCode,'/',dataType,'Data'],'x','y')  
%     % get kernel matrices
%     [K,Da,Da1,Lab] = getKernelMatrices(xTrain,x,kernelType,kernelParam); 
%     save(['../nls_data/expData/',expCode,'/',dataType,'Kernels',num2str(dataLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'],'K','Da','Da1','Lab')
%   end
end


  