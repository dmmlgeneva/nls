function [a,b,selected,convergenceHistory,elapsedTime] = algo_ADMM_eNet(matrices,y,mu,muENet,tau,silent)
% ALGO_ADMM_ENET - admm algo to solve kernel ls with non-linaer elastic net regulariser
%   
% INPUTS
%   matrices - struct with either 
%     - either precalucalted K,F,Z,Lc,Rc,ZTZ algo matrices see alo tech paper
%     - or the original kernel gram matrices K,Da,Lab       
%   y - outptus
%   tau - hyperparameter
% OUTPUTS
%   a,b - model parameters
%   selected - selected dimensions
%   convergenceHistory, elapsedTime - monitoring of admm convergence
% 
% EXAMPLE: [a,b,selected,convergenceHistory,elapsedTime] = algo_ADMM_eNet(K,F,Z,Lc,Rc,ZTZ,y,0.1,0.1)
%
% CREATED: MG - 22/05/2017
% MG - 4/10/2017 - find the minimum norm solution in step1
% MG 7/10/2017: change of logic in the ADMM solving
% MG 10/10/2017: change the convergence criteria to the standard one


% supress warnings about nearly singular matrix in linear problems;
warning('off','MATLAB:nearlySingularMatrix')
warning('off','MATLAB:singularMatrix')

% fill in optional parameters
if ~exist('silent','var') || isempty(silent),
  silent = 1;
end

%% initialise algo 
%%%%%%%%%%%%%%%%%%%

% initial constants
n = size(matrices.F,1); d = (size(matrices.F,2)-n)/n;
maxIter = 1000;
minChange = 1e-4;
eabs = 1e-4; erel=1e-4;

% objective function
J1 = @(cTemp) 1/n*norm(y-matrices.F*cTemp)^2 ;
J2 = @(cTemp) tau*muENet*sum(sqrt(sum(reshape(matrices.Z*cTemp,n,[]).^2)))/sqrt(n);
J3 = @(cTemp) tau*(1-muENet)*norm(matrices.Z*cTemp)^2/n + mu*cTemp'*matrices.Q*cTemp;
J = @(cTemp) J1(cTemp) + J2(cTemp) + J3(cTemp); 

% ADMM primal and dual functions
rprim = @(cTemp,vTemp) norm(matrices.Z*cTemp-vTemp); % primal residual
rdual = @(vTemp,vold,rhoTemp) norm(rhoTemp*matrices.Z'*(vTemp-vold)); % dual residual

% ADMM steps optimisation functions
S1 = @(cTemp,rhoTemp,vTemp,lamTemp) J1(cTemp)+J3(cTemp) + rhoTemp/2*norm(matrices.Z*cTemp - vTemp + lamTemp)^2;
% watch out, in S2 the l1 norm is with respect to vTemp not c
S2 = @(cTemp,rhoTemp,vTemp,lamTemp) tau*muENet*sum(sqrt(sum(reshape(v,n,[]).^2)))/sqrt(n) + rhoTemp/2*norm(matrices.Z*cTemp - vTemp + lamTemp)^2;
S3 = @(cTemp,rhoTemp,vTemp,lamTemp) norm(matrices.Z*cTemp - vTemp + lamTemp)^2*rhoTemp/2;
LHS = matrices.LHS + ((1-muENet)*2*tau/n)*matrices.ZTZ;
gradFunc = @(cTemp,rhoTemp,vTemp,lamTemp) (LHS + rhoTemp*matrices.ZTZ)*cTemp - matrices.RHS + rhoTemp*matrices.Z'*(lamTemp-vTemp);


% pre-allocate solution matrices
c = zeros(n+n*d,maxIter); v = zeros(n*d,maxIter);
lambda = zeros(n*d,maxIter);
slct = ones(d,maxIter);

% initiate algo values
c=zeros(n+n*d,maxIter); 
c(:,1) = pinv(LHS)*matrices.RHS;
v=zeros(n*d,maxIter); v(:,1) = matrices.Z*c(:,1);
slct = zeros(d,maxIter); slct(:,1) = sqrt(sum(reshape(v(:,1),n,[]).^2));
rho = 0.1; 
convergenceHistory = zeros(maxIter,6);
convergenceHistory(1,1)=rprim(c(:,1),v(:,1));
convergenceHistory(1,2)=rdual(v(:,1),v(:,1),rho);
convergenceHistory(1,3) = J(c(:,1));
convergenceHistory(1,4) = rho;
convergenceHistory(1,5) = norm(v(:,1));
convergenceHistory(1,6) = norm(c(:,1));


%% do the ADMM descent
%%%%%%%%%%%%%%%%%%%%%%
iter = 1; admm = tic;

while iter<maxIter
  iter = iter+1;

  % step 1: solve for c
  % instead of solving completely (expensive and not stable)
  %c(:,iter) = [LHS + rho*matrices.ZTZ]\[matrices.RHS + rho*matrices.Z'*(v(:,iter-1)-lambda(:,iter-1))];
  % do a few gradient steps
  cOld = c(:,iter-1); cNew = cOld; alpha = 10; itr = 0;
  while itr < 10
    itr = itr+1;
    grad = gradFunc(cOld,rho,v(:,iter-1),lambda(:,iter-1));
    while alpha > 1e-12
      cNew = cOld - alpha*grad;
      if S1(cNew,rho,v(:,iter-1),lambda(:,iter-1)) < S1(cOld,rho,v(:,iter-1),lambda(:,iter-1))
        cOld = cNew;
        break
      else
        alpha = alpha/2;
        cNew = cOld;
      end
    end
  end
  c(:,iter) = cNew;


  % step 2: solve for v (vectorised) (same as for L1)
  tr = tau*muENet/ ( rho*sqrt(n));
  Zc = matrices.Z*c(:,iter);
  proxCenter = Zc+lambda(:,iter-1);
  proxCenterMat = reshape(proxCenter,n,[]);
  vMat = bsxfun( @times, proxCenterMat , max( 0, (1 - tr./ sqrt(sum((proxCenterMat.^2))) ) ) );
  v(:,iter)=vMat(:);
  % get selected variables
  slct(:,iter) = sqrt(sum(vMat.^2))/sqrt(n);


  % step 3: update dual variable
  lambda(:,iter) = lambda(:,iter-1) + Zc - v(:,iter);

  % update convergence monitoring
  convergenceHistory(iter,1)=rprim(c(:,iter),v(:,iter));
  convergenceHistory(iter,2)=rdual(v(:,iter),v(:,iter-1),rho);
  convergenceHistory(iter,3) = J(c(:,iter));
  convergenceHistory(iter,4) = rho;
  convergenceHistory(iter,5) = sum(slct(:,iter));
  convergenceHistory(iter,6) = norm(c(:,iter));
  

  % update rho (see Boyd2010 equation 3.13 + fix to constant if becomes circular 
  if convergenceHistory(iter,1) > 10*convergenceHistory(iter,2)
    rho = 2*rho; 
    lambda(:,iter) = lambda(:,iter)/2; % and update the dual variable (see Boyd2010 end of the 3.13 section)
  elseif convergenceHistory(iter,2) > 10*convergenceHistory(iter,1)
    rho = rho/2;
    lambda(:,iter) = lambda(:,iter)*2; % and update the dual variable (see Boyd2010 end of the 3.13 section)
  end
  
  % report progress
  if ~silent
      fprintf('algo_ADMM_eNet tau=%f, numIter=%d, obj=%f, conv=%f \n',tau,iter,convergenceHistory(iter,3),convergenceHistory(iter,5))
  end


  %% check convergence (see Boyd2017 3.12)
  epri = sqrt(n*d)*eabs + erel*max(norm(Zc),norm(v(:,iter)));
  edual = sqrt(n+n*d)*eabs + erel*norm(matrices.Z'*lambda(:,iter)*rho);
  
  if iter>5 && ...
      convergenceHistory(iter,1) < epri && ...
      convergenceHistory(iter,2) < edual
      convergenceHistory(iter+1:end,:)=[];
    break
  end 


end

% prepare output variables results
elapsedTime = toc(admm);
a = c(1:n,iter);
b = c(n+1:end,iter);
selected = slct(:,iter);

end