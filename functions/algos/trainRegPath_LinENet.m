function [a,selected,convergenceHistory,elapsedTime]=trainRegPath_LinENet(x,y,mu,tauGrid,silent)
% TRAINREGPATH_LINL1 - train the full regularization path for tauGrid using linear L1L2 algo
%   from PASPAL toolbox (PASPAL Copyright 2010, Sofia Mosci and Lorenzo Rosasco)
%   
% INPUTS
%   x - train data inputs
%   y - train data outputs
%   mu - weighting of l1 and l2
%   tauGrid - struct with min,max,length
%   silent - if 1 will not printout progress
% OUTPUTS
%   cells for every tau in grid
%   a - model parameters
%   selected - selected parameters (for consistency with nolinear methods selected = theta)
%   convergenceHistory, elapsedTime - monitoring of algo convergence
% 
% EXAMPLE: [a,b,selected,convergenceHistory,elapsedTime] = trainRegPath_LinENet(x,y,0.9,[0.1 0.2 0.3],0)
%
% CREATED: MG - 3/05/2017
% MG 25/9/2017 dropped parfor processing

%% fill in optional parameters
if ~exist('silent','var') || isempty(silent),
  silent = 0;
end
%% prepare param grid
if isstruct(tauGrid)
  paramGrid = logspace(tauGrid.min,tauGrid.max,tauGrid.length);
else
  paramGrid = tauGrid;
end

%% get the whole regularization path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
paramGrid_Length = length(paramGrid);
% prealocate cells;
a = cell(1,paramGrid_Length);
selected = cell(1,paramGrid_Length); convergenceHistory = cell(1,paramGrid_Length);
elapsedTime = cell(1,paramGrid_Length); 

% if isdeployed % do parfor looping
%   % create a temporary directory for parcluster, this is removed automatically at the end of execution 
%   t=tempname(); 
%   mkdir(t); 
%   % some cleanup 
%   c=parcluster('local'); 
%   c.JobStorageLocation=t; 
%   delete(c.Jobs); 
%   % get the number of cores allocated by slurm 
%   cpus=str2num(getenv('SLURM_CPUS_PER_TASK')); 
%   % create a parpool 
%   parpool(c, cpus);
%   
%   % get solutions for all hyper-param values
%   parfor pIdx=1:paramGrid_Length;
%     tic;
%     [a{pIdx},convergenceHistory{pIdx}] = l1l2_algorithm(x,y,paramGrid(pIdx),mu);
%     selected{pIdx} = a{pIdx}; % for consistency with nonlinear methods
%     elapsedTime{pIdx} = toc; % for consistency with nonlinear methods
%     if ~silent
%       fprintf('train linear eNet pIdx=%d, numIter=%d, time=%.2f \n',pIdx,size(convergenceHistory{pIdx},1),elapsedTime{pIdx})
%     end
%   end
%   delete(gcp('nocreate'))
% else % if not deployed do simple for loop
  for pIdx=1:paramGrid_Length
    tic;
    [a{pIdx},convergenceHistory{pIdx}] = l1l2_algorithm(x,y,paramGrid(pIdx),mu);
    selected{pIdx} = a{pIdx}; % for consistency with nonlinear methods
    elapsedTime{pIdx} = toc; % for consistency with nonlinear methods
    if ~silent
      fprintf('train linear eNet pIdx=%d, numIter=%d, time=%.2f \n',pIdx,size(convergenceHistory{pIdx},1),elapsedTime{pIdx})
    end
  end
%end

end
