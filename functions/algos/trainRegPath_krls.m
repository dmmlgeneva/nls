function [a,elapsedTime]=trainRegPath_krls(K,y,tauGrid,silent)
% TRAINREGPATH_KRLS - train the full regularization path for tauGrid
% using standard krls (hilbert norm regularized least squares)
%   
% INPUTS
%   K - kernel gram matrix
%   tauGrid - struct with min,max,length
%   silent - if 1 will not printout progress
% OUTPUTS
%   a - cell for every tau of model parameters
%   elapsedTime - monitoring of time elapsed
% 
% EXAMPLE: [a,elapsedTime] = trainRegPath_krls(K,tauGrid,0)
%
% CREATED: MG - 17/05/2017

%% fill in optional parameters
if ~exist('silent','var') || isempty(silent),
  silent = 0;
end
%% prepare param grid
if isstruct(tauGrid)
  paramGrid = logspace(tauGrid.min,tauGrid.max,tauGrid.length);
else
  paramGrid = tauGrid;
end

%% get the whole regularization path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
paramGrid_Length = length(paramGrid);
n = size(K,1);
% prealocate cells;
a = cell(1,paramGrid_Length); 
elapsedTime = cell(1,paramGrid_Length); 

% simple enough so no need to paralelize the forloop
for pIdx=1:paramGrid_Length
  tic
  a{pIdx} = (K + n*paramGrid(pIdx)*eye(n))\y;
  elapsedTime{pIdx} = toc;
  if ~silent
    fprintf('train krls pIdx=%d, time=%.2f \n',pIdx,elapsedTime{pIdx})
  end
end

end
