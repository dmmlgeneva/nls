function [a,b,selected,convergenceHistory,elapsedTime]=trainRegPath_eNet(matrices,y,mu,muENet,tauGrid,silent)
% TRAINREGPATH_eNet - train the full regularization path for tauGrid using non-linear elastic net algo
%   
% INPUTS
%   matrices - struct with algo input matrices
%   y - train data outputs
%   mu - weighting of l1 and l2
%   tauGrid - struct with min,max,length
%   silent - if 1 will not printout progress
% OUTPUTS
%   cells for every tau in grid
%   a,b - model parameters
%   selected - avg norms of v vectors in the algo
%   convergenceHistory, elapsedTime - monitoring of admm convergence
% 
% EXAMPLE: [a,b,selected,convergenceHistory,elapsedTime] = trainRegPath_eNet(matrices,y,0.3,[0.1 0.2 0.3],0)
%
% CREATED: MG - 18/05/2017

%% fill in optional parameters
if ~exist('silent','var') || isempty(silent),
  silent = 0;
end
%% prepare param grid
if isstruct(tauGrid)
  paramGrid = logspace(tauGrid.min,tauGrid.max,tauGrid.length);
else
  paramGrid = tauGrid;
end

%% get the whole regularization path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
paramGrid_Length = length(paramGrid);
% prealocate cells;
a = cell(1,paramGrid_Length); b = cell(1,paramGrid_Length);
selected = cell(1,paramGrid_Length); convergenceHistory = cell(1,paramGrid_Length);
elapsedTime = cell(1,paramGrid_Length); 

if isdeployed % do parfor looping
  % create a temporary directory for parcluster, this is removed automatically at the end of execution 
  t=tempname(); 
  mkdir(t); 
  % some cleanup 
  c=parcluster('local'); 
  c.JobStorageLocation=t; 
  delete(c.Jobs); 
  % get the number of cores allocated by slurm 
  cpus=str2num(getenv('SLURM_CPUS_PER_TASK')); 
  % create a parpool 
  parpool(c, cpus);
  
  % get solutions for all hyper-param values
  parfor pIdx=1:paramGrid_Length;
    [a{pIdx},b{pIdx},selected{pIdx},convergenceHistory{pIdx},elapsedTime{pIdx}] = algo_ADMM_eNet(matrices,y,mu,muENet,paramGrid(pIdx));
    if ~silent
      fprintf('train eNet pIdx=%d, numIter=%d, time=%.2f \n',pIdx,size(convergenceHistory{pIdx},1),elapsedTime{pIdx})
    end
  end
  delete(gcp('nocreate'))
else % if not deployed do simple for loop
  for pIdx=1:paramGrid_Length
    [a{pIdx},b{pIdx},selected{pIdx},convergenceHistory{pIdx},elapsedTime{pIdx}] = algo_ADMM_eNet(matrices,y,mu,muENet,paramGrid(pIdx),0);
    if ~silent
      fprintf('train eNet pIdx=%d, numIter=%d, time=%.2f \n',pIdx,size(convergenceHistory{pIdx},1),elapsedTime{pIdx})
    end
  end
end

end
