function [a,b,selected,numIter,elapsedTime]=trainRegPath_denovas1(data,mu,tauGrid,silent)
% TRAINREGPATH_DENOVAS1 - train the full regularization path for tauGrid
% using denovas algo with 1 step only
% see Rosasco et al:Nonparametric sparsity and Regularization (2013)
%   
% INPUTS
%   data - struct with algo input matrices
%   mu - the hilbert norm hyper param
%   tauGrid - struct with min,max,length
%   silent - if 1 will not printout progress
% OUTPUTS
%   cells for every mu and tau combination
%   a,b - model parameters
%   numIter, elapsedTime - monitoring of algo
% 
% EXAMPLE: [a,b,selected,numIter,elapsedTime] = trainRegPath_denovas1(data,0.5,tauGrid,0)
%
% CREATED: MG - 12/05/2017

%% fill in optional parameters
if ~exist('silent','var') || isempty(silent),
  silent = 0;
end
%% prepare param grid
if isstruct(tauGrid)
  paramGrid = logspace(tauGrid.min,tauGrid.max,tauGrid.length);
else
  paramGrid = tauGrid;
end

%% initiate denovas constants
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
stop_par.max_iter_ext = 1e5;
stop_par.max_iter_int = 1e3;
stop_par.tol_ext = 1e-6; 
stop_par.tol_int = 1e-3; 

n = length(data.K);
d = length(data.L)/n;
% step sizes
sigma = normest(data.K)/n;
eta = 1/normest(data.L);

%% get the whole regularization path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
paramGrid_Length = length(paramGrid);
% prealocate cells;
a = cell(1,paramGrid_Length); b = cell(1,paramGrid_Length);
selected = cell(1,paramGrid_Length); 
numIter = cell(1,paramGrid_Length);
elapsedTime = cell(1,paramGrid_Length); 

if isdeployed % do parfor looping
  % create a temporary directory for parcluster, this is removed automatically at the end of execution 
  t=tempname(); 
  mkdir(t); 
  % some cleanup 
  c=parcluster('local'); 
  c.JobStorageLocation=t; 
  delete(c.Jobs); 
  % get the number of cores allocated by slurm 
  cpus=str2num(getenv('SLURM_CPUS_PER_TASK')); 
  % create a parpool 
  parpool(c, cpus);
  
  % get solutions for all hyper-param values
  parfor pIdx=1:paramGrid_Length;
    tic
    [a{pIdx},b{pIdx},numIter{pIdx},selected{pIdx}] = nvs_algorithm(data,paramGrid(pIdx),mu*paramGrid(pIdx),[],[],sigma,eta,stop_par);
    elapsedTime{pIdx} = toc;
    if ~silent
      fprintf('train denovas1 pIdx=%d, numIter=%d, time=%.2f \n',pIdx,numIter{pIdx},elapsedTime{pIdx})
    end
  end
  delete(gcp('nocreate'))
else % if not deployed do simple for loop
  for pIdx=1:paramGrid_Length
    tic;
    [a{pIdx},b{pIdx},numIter{pIdx},selected{pIdx}] = nvs_algorithm(data,paramGrid(pIdx),mu*paramGrid(pIdx),[],[],sigma,eta,stop_par);
    elapsedTime{pIdx} = toc;
    if ~silent
      fprintf('train denovas1 pIdx=%d, numIter=%d, time=%.2f \n',pIdx,numIter{pIdx},elapsedTime{pIdx})
    end
  end
end

end
