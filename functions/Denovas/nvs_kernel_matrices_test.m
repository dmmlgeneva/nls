function[K,Z] = nvs_kernel_matrices_test(X,Xtest,ker,ker_par)
%NVS_KERNEL_MATRICES_TEST Creates kernel matrices
%   [K,Z] = NVS_KERNEL_MATRICES_TEST(X,XTEST,KER,KER_PAR) Creates the kernel
%   matrices necessary for testing NVS solution with trainig matrix X and
%   test matrix Xtest.
%
% NB: per Z, sul secondo indice le prime n componenti si riferiscono alla 
% variabile 1, le seconde n alla variabile 2 e cosi' via

if nargin<3; error('too few inputs!'); end
if and(nargin<4,~strcmp(ker,'lin'));  error('kernel parameter is missing!'); end
if nargin>4; error('too many inputs!'); end

switch ker
    case 'lin'
        Z = z_lin(X,Xtest);
        K = k_lin(X,Xtest);
    case 'gauss'
        fprintf('\n building matrix Z...')
        Z = z_gauss(X,Xtest,ker_par);
        fprintf('\t matrix Z built\n')
        fprintf('\n building matrix K...')
        K = k_gauss(X,Xtest,ker_par);
        fprintf('\t matrix K built\n')
    case 'pol' % same as poly for back compatibility
        fprintf('\n building matrix Z...')
        Z = z_pol(X,Xtest,ker_par);
        fprintf('\t matrix Z built\n')
        fprintf('\n building matrix K...')
        K = k_pol(X,Xtest,ker_par);
        fprintf('\t matrix K built\n')
    case 'poly'
        fprintf('\n building matrix Z...')
        Z = z_pol(X,Xtest,ker_par);
        fprintf('\t matrix Z built\n')
        fprintf('\n building matrix K...')
        K = k_pol(X,Xtest,ker_par);
        fprintf('\t matrix K built\n')
    otherwise
        error('Unknown kernel')
end

function K = k_gauss(X,Xtest,sigma)
n = size(X,1);
ntest = size(Xtest,1);
K = zeros(ntest,n);
var=(2*(sigma^2));
for i = 1:ntest; 
    for j=1:n;
        K(i,j) = exp(-norm(Xtest(i,:)-X(j,:))^2/var);
    end; 
end;

function Z = z_gauss(X,Xtest,sigma)
[n,d] = size(X);
ntest = size(Xtest,1);
Z = zeros(ntest,n*d);
for i = 1:ntest; 
    for a=1:d; 
        for j=1:n;
            Z(i,(a-1)*n+j) = 1/sigma^2*(Xtest(i,a)-X(j,a))*exp(-1/(2*sigma^2)*norm(Xtest(i,:)-X(j,:))^2);
        end; 
    end; 
end;

function K = k_pol(X,Xtest,p)
K = (Xtest*X'+1).^p;

function Z = z_pol(X,Xtest,p)
[n,d] = size(X);
ntest = size(Xtest,1);
Z = zeros(ntest,n*d);
for i = 1:ntest; 
    for a=1:d; 
        for j=1:n; 
            Z(i,(a-1)*n+j) = p*(sum(Xtest(i,:).*X(j,:))+1)^(p-1)*Xtest(i,a); 
        end; 
    end; 
end


function K = k_lin(X,Xtest)
K = Xtest*X';

function Z = z_lin(X,Xtest)
[n,d] = size(X);
ntest = size(Xtest,1);
Z = zeros(ntest,n*d);
for i = 1:ntest; 
    for a=1:d; 
        for j=1:n; 
            Z(i,(a-1)*n+j) = Xtest(i,a); 
        end; 
    end; 
end
