function [selected,k,tmin,f1,f2] = nvs_regpath(data,Y,tau_values,varargin)
% NVS_SERIES acceleration of nvs through cascade of nvs w. decreasing 
% values of TAU_VALUES
% 
% [SELECTED] = NVS_REGPATH(DATA,Y,KER,KER_PAR,TAU_VALUES) for each value in TAU_VALUES evaluates 
%   nvs solution with smoothness parameter 0, and builds cell of indexes 
%   of selected features. DATA can be either the nxd input matrix or a
%   struct with fields K Z L corresponding to the kernel matrices. Y is the
%   nx1 label vector.
% 
% [SELECTED,K] = NVS_REGPATH(DATA,Y,KER,KER_PAR,TAU_VALUES) also returns a 
%   vector with the number of iterations for each value in TAU_VALUES
% 
% [SELECTED,K,TMIN] = NVS_REGPATH(DATA,Y,KER,KER_PAR,TAU_VALUES) also 
%   returns the index corresponding at the minimum value of tau_values
%   evalueated
% 
% [...] = NVS_REGPATH(DATA,Y,KER,KER_PAR,TAU_VALUES,MU_FACT) sets l2 parameter equal to
%   MU_FACT*SIGMA0
% 
% [...] = NVS_REGPATH(DATA,Y,KER,KER_PAR,TAU_VALUES,MU_FACT,SIGMA) uses SIGMA+MU as 
%   step size
% 
% [...] = NVS_REGPATH(DATA,Y,KER,KER_PAR,TAU_VALUES,MU_FACT,SIGMA,STOP_PAR) 
%   stops according to the fields MAX_ITER_EXT, MAX_ITER_INT, TOL_EXT, 
%   TOL_INT in STOP_PAR. if TOL_EXT and TOL_INT are scalars, uses them as 
%   tolerances for stopping the iterations. If TOL_EXT and TOL_INT are 2x1 
%   vectors, uses TOL_EXT(1) and TOL_INT(1) as tolerances for computing an
%   approximated regularization path until tau_values(1), then re-evaluates
%   the solution for tau=tau_values(1) with tolerance TOL_EXT(2)(<TOL_INT(1)) AND 
%   TOL_INT(2)(<TOL_INT(1)).In the latter cases the outputs correspond to 
%   the tau_values(1) only.

    if nargin<3; error('too few inputs!'); end
    smooth_par = 0;
    ker = 'pol';
    ker_par = 2;
    max_iter_ext = 1e5;
    max_iter_int = 1e3;
    tol_ext = 1e-6; 
    tol_int = 1e-3; 
    all_path = true;
        
    % OPTIONAL PARAMETERS
    args = varargin;
    nargs = length(args);
    for i=1:2:nargs
        switch args{i},		
            case 'kernel'
                ker = args{i+1};
            case 'kernel_par'
                ker_par = args{i+1};	
            case 'smooth_par'
                smooth_par = args{i+1};
            case 'max_iter_ext'
                max_iter_ext = args{i+1};
            case 'max_iter_int'
                max_iter_int = args{i+1};
            case 'tolerance_int'
                tol_int = args{i+1};
            case 'tolerance_ext'
                tol_ext = args{i+1};
            case 'all_path'
                all_path = args{i+1};
        end
    end
    
    ntau = length(tau_values);
    if isnumeric(data);
        [n,d] = size(data);  % number of training points
        fprintf('\nbuilding kernel matrices...\n')
        [K,Z,L] = nvs_kernel_matrices(data,ker,ker_par);
        matrices = struct('Y',Y,'K',K,'Z',Z,'L',L);
        clear K Z L;
        fprintf('\nkernel matrices completed\n')
        data = matrices;
    else
        n = length(data.K);
        d = round(length(data.L)/n);
    end
    data.Y = Y;    
    
    
    % if interested in just the first value of TAU_VALUES, evaluates solutions
    % for larger vaues of tau with looser tolerance, as warm start for
    % TAU_VALUES(1)
    tol_ext = ones(ntau,1).*tol_ext;
    if ~all_path;
        tol_ext(2:end) = tol_ext(2:end).*100;
    end
    stop_par.tol_int = tol_int;
    stop_par.max_iter_ext = max_iter_ext;
    stop_par.max_iter_int = max_iter_int;
    
    % step sizes
    sigma = normest(data.K)/n;
    eta = 1/normest(data.L);
    
    selected = ones(d,ntau);
    k = zeros(ntau,1);
    if nargout>3;
        f1tot = cell(ntau,1);
        f2tot = cell(ntau,1);
    end
    
    tau_min_reached = false;
    
    % initialization 
    f0.f01 = zeros(n,1);
    f0.f02 = zeros(n*d,1);
    v0 = zeros(n*d,1);
    t = 0;
    fprintf('\n evaluating the regularization path...')
    fprintf('\ntau =')
    while and(~tau_min_reached,t<ntau);
        t = t+1;
        fprintf('\n%d,',t)
        stop_par.tol_ext = tol_ext(ntau+1-t);
        % evaluates NVS solution for decreasing values of tau_values
        [f1,f2,k(ntau+1-t),selected(:,ntau+1-t),v] = nvs_algorithm(data,tau_values(ntau+1-t),smooth_par,f0,v0,sigma,eta,stop_par);
%         disp(find(selected(:,ntau+1-t))')
        % re-initializes vector (f01,f02)(k,z)
        f0.f01 = f1;
        f0.f02 = f2;
        v0 =v;
        % if all variables are already selected exit
        if sum(selected(:,ntau+1-t))==min(d,n); 
            tau_min_reached = true; 
        end
        if nargout>3;
            f1tot{ntau+1-t} = f1;
            f2tot{ntau+1-t} = f2;
        end
    end
    
    tmin = ntau+1-t;
    
    if and(nargout>3,all_path);
        f1 = f1tot;
        f2 = f2tot;
    end

    if ~all_path;
        if nargout>3, 
            f1 = f1tot{1}; 
            f2 = f2tot{1}; 
        end
        selected = selected(:,1);
    end
    
end