function [f1,f2,k,selected,v] = nvs_algorithm(data,tau,mu_fact,f0,v0,sigma,eta,stop_par)
% NVS_ALGORITHM argmin of least squares error with NVS penalty computed via
% FISTA
% 
% [F1,F2] = NVS_ALGORITHM(DATA,TAU) returns the solution of NVS 
%   regularization with sparsity parameter TAU and smoothness parameter 0. 
%   DATA is a structure containing the fields:
%   -Y: the Nx1 label vector, 
%   -K: the kernel matrix, 
%   -Z: the Nx(N*D) matrix of kernel derivatives, 
%   -L: the (N*D)X(N*D) matrix of the second derivatives of the kernel.
% 
% [F1,F2] = NVS_ALGORITHM(DATA,TAU,MU_FACT) returns the solution of
%   NVS regularization with sparsity parameter TAU and smoothness parameter
%   MU_FACT *SIGMA0.
% 
% [F1,F2] = NVS_ALGORITHM(DATA,TAU,MU_FACT,F0) use the Nx1 vector F0.F01 
%   and the (N*D)x1 vector F0.F02 as initialization vectors for F1 F2.
%
% [F1,F2] = NVS_ALGORITHM(DATA,TAU,MU_FACT,F0,V0) use the NDx1 vector V0
%   as initialization vector for the first inner iteration.
% 
% [F1,F2] = NVS_ALGORITHM(DATA,TAU,MU_FACT,F0,V0,SIGMA) uses 
%   SIGMA+MU as step size
% 
% [F1,F2] = NVS_ALGORITHM(DATA,TAU,MU_FACT,F0,V0,SIGMA,ETA) uses 
%   ETA as step size for the inner iteration
% 
% [F1,F2] = NVS_ALGORITHM(DATA,TAU,MU_FACT,F0,V0,SIGMA,ETA,STOP_PAR) stops
%   according to the fileds MAX_ITER_EXT, MAX_ITER_INT, TOL_EXT, TOL_INT in
%   STOP_PAR. If some of these fields are missing, then uses default
%   values.
% 
% [F1,F2,K] = NVS_ALGORITHM(...) also returns the number of iterations
% 
% [F1,F2,K,SELECTED] = NVS_ALGORITHM(...) also returns the a logical vector
% encoding which variables satisfy ||D_jf||>0.
% 
% [F1,F2,K,SELECTED,V] = NVS_ALGORITHM(...) also returns last output of the
% inner iteration.

    if nargin<2; error('too few inputs!'); end
    if nargin<3; mu_fact = 0; end
    if nargin<4; f0 = []; end
    if nargin<5; v0 = []; end
    if nargin<6; sigma=[]; end
    if nargin<7; eta=[]; end
    if nargin<8; stop_par = struct(); end
    if nargin>8; error('too many inputs!'); end

    if isfield(stop_par,'max_iter_ext');
        max_iter_ext = stop_par.max_iter_ext;
    else
        max_iter_ext = 1e5;
    end
    if isfield(stop_par,'max_iter_int');
        max_iter_int = stop_par.max_iter_int;
    else
        max_iter_int = 1e5;
    end
    if isfield(stop_par,'tol_ext');
        tol_ext = stop_par.tol_ext;
    else
        tol_ext = 1e-6;
    end
    if isfield(stop_par,'tol_int');
        tol_int = stop_par.tol_int;
    else
        tol_int = 1e-3;
    end
    clear stop_par;
    
    Y = data.Y;
    K = data.K;
    Z = data.Z;
    L = data.L;
    clear data;   

    n =length(Y);  % number of training points
    d = round(length(L)/n); % number of variables
    
    % initialization vectors
    if isempty(f0); 
        f01 = zeros(n,1); 
        f02 = zeros(n*d,1); 
    else
        f01 = f0.f01;
        f02 = f0.f02;
    end
    clear f0;
    
    if isempty(v0);
        v0 = zeros(n*d,1);
    end
    
    % step size for outer iteration
    if isempty(sigma);
        a = normest(K);
        sigma = a/n;
    end

    % step size for inner projection
    if isempty(eta);
        eta = 1/normest(L);
    end

    % useful quantities
    mu = mu_fact*sigma;
    step = sigma+mu;
    mu_fact = 1-mu/step;   
    R = tau/(step*sqrt(n)); %threshold for inner projection
    
    % initialization
    k = 0; % iteration of outer loop
    c = 1;
    t = .5;
    % initialize vectors    
    f1 = f01;
    f2 = f02;
    h1 = f01;
    h2 = f02;
    v = v0;
    % initialize multiplication of vector with matrices
    Kf = K*f1;
    Zf = Z*f2;
    Lf = L*f2;
    Kfp = Kf;
    Zfp = Zf;
    Lh = Lf.*mu_fact;
    % initial value of the functional
    E = norm(Kf+Zf-Y)^2/n + sum(sqrt(sum(reshape((Z'*f1+Lf).^2,n,d),1)))*2*tau/sqrt(n)+mu*(f2'*(Lf+2*(Z'*f1))+f1'*Kf);
    Es = E*2*ones(5,1); % vector of 5 previuos values of E
    
    stop = 0;
    c_null = 0;
    
    %fprintf('q_int=')
    while and(k<max_iter_ext,~stop)
        k = k+1;
        
        f1_prev = f1;                    
        f2_prev = f2;

        tp = t;
        t = (-c +sqrt(c^2+8*c))/4;
        fact1 = (1-t+t/tp);
        fact2 = ((tp-1)*(t/tp));
        
        Es(mod(k,5)+1) = E; % store last 5 values of the functional
        
        f1 = h1.*mu_fact + (Y - Kf.*fact1- Kfp.*fact2 - Zf.*fact1 - Zfp.*fact2)./(step*n);
        
        Kfp = Kf;
        Zfp = Zf;
        Lfp = Lf;
        
        ZTf = Z'*f1;
        Kf = K*f1;
        normf = (h2.*mu_fact)'*(2*ZTf+Lh) + (f1'*Kf);

        [v,Lv,null_solution] = nvs_projection(ZTf,Lh,normf,n,d,L,R,eta,v,max_iter_int,k^(-3/2)*tol_int);
        f2 = h2.*mu_fact - v;
        
        %count the number of consequent iterations with null solution
        c_null = null_solution*(c_null+null_solution); 
        % check if the solution has been null for two consequent iterations
        if c_null>1, break, end 

        Zf = Z*f2;
        Lf = Lh-Lv;
        
        h1 = f1.*fact1 + f1_prev.*fact2;
        h2 = f2.*fact1 + f2_prev.*fact2;
        
        Lh = Lf.*(mu_fact*fact1)+Lfp.*(mu_fact*fact2); %needed as input to nvs_projection
        
        E = norm(Kf+Zf-Y)^2/n + sum(sqrt(sum(reshape((ZTf+Lf).^2,n,d),1)))*2*tau/sqrt(n)+mu*(f2'*(Lf+2*ZTf)+f1'*Kf);

        stop = abs(mean(Es)-E)<mean(Es)*tol_ext;

        c  = (1-t)*c; 
        
    end

    if nargout>3; % finds relevant indexes
        selected = [sqrt(sum(reshape(v.^2,n,d),1))>R*(1-tol_int)]';
    end
end

function [v,Lv,null_solution,q] = nvs_projection(ZTf,Lf,normf,n,d,L,R,eta,v0,max_iter_int,tol_int)
    tol_nul = tol_int;
    w = eta*(ZTf+ Lf);
    
    % initialization
    v = v0;
    h = v;
    s = .5;
    c = 1;
    q = 0;
    Lv = L*v;
    Lh = Lv;
    dist_proj_prev = 0;
    stop = 0;
    while and(~stop,q<max_iter_int);
        q = q+1;
                
        v_prev = v;
        Lv_prev = Lv;
        
        sp = s;
        s = (-c + sqrt(c^2+8*c))/4;
        fact1 = (1-s+s/sp);
        fact2 = (sp-1)*(s/sp);
        
        v_noproj = h - eta*(Lh) + w;
        norme = sqrt(sum(reshape(v_noproj.^2,n,d),1));
        irrelevant = norme<R*(1-tol_nul);
        scal_fact = repmat(min(1,R./norme),n,1); 
        v = v_noproj.*scal_fact(:);
        
        Lv = L*v;
        h = v.*fact1 + v_prev.*fact2;
        Lh = Lv.*fact1 + Lv_prev.*fact2;
                
        dist_proj = (Lv-2*Lf-2*ZTf)'*v;
        stop = abs(dist_proj-dist_proj_prev)<=(dist_proj_prev+normf)*tol_int;
        dist_proj_prev = dist_proj;
        c = (1-s)*c;

    end
    
    null_solution = all(irrelevant);
    %fprintf('%d,',q)
end