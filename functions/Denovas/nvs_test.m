function [test_err,Ytest_learned] = nvs_test(Ktest,Ztest,Ytest,alpha,beta,err_type)
% NVS_TEST prediction error for linear model
%   [TEST_ERR] = NVS_TEST(KTEST,ZTest,YTEST,ALPHA,BETA) returns the classification 
%   error committed by ALPHA,BETA on the test data KTEST, ZTEST, YTEST.
% 
%   [TEST_ERR,YTEST_LEARNED] = NVS_TEST(KTEST,ZTest,YTEST,ALPHA,BETA) also returns the
%   estimated value for the test samples.
%
if nargin<6; err_type = 'regr'; end
if nargin>6; error('too many inputs!'); end

Ytest_learned = Ktest*alpha+Ztest*beta;

% if class weight is not given let false positive and false negative 
% weight the same error     
if strcmp(err_type,'class'); 
    npos = sum(Ytest>0);
    nneg = sum(Ytest<0);
    err_type = [npos/(npos+nneg) nneg/(npos+nneg)]; 
end

% if class weight is given (e.g. ERR_TYPE = [1/2 1/2]) compute FP and 
% FN rate and then compute weighted mean with CLASS_FRACTION
if isnumeric(err_type)
    class_fraction = err_type;

    Ytest_learned = sign(Ytest_learned);
    test_err = 0;
    if npos>0;
        err_pos = sum((Ytest_learned(Ytest>0)~=sign(Ytest(Ytest>0))))/npos;
        test_err = test_err + err_pos*max(class_fraction(1),nneg==0);
    end
    if nneg>0;
        err_neg = sum((Ytest_learned(Ytest<0)~=sign(Ytest(Ytest<0))))/nneg;
        test_err = test_err + err_neg*max(class_fraction(2),npos==0);
    end        
elseif isequal(err_type,'regr')
    test_err = norm(Ytest_learned-(Ytest))^2/length(Ytest);
end
