function cp_derivReal()
% CP_MSEREAL - compare sparsity over test
%
%
% EXAMPLE: cp_sparsity()
%
% CREATED: MG - 12/10/2017
% MG Watch-out: Unlike the synthetic exp, this assumes only 1 train size 

%% nicer colors
colorOrder=[0.6350    0.0780    0.1840; % tmave cervena
    0.4660    0.6740    0.1880; % zelena
    0.8500    0.3250    0.0980; % cervena
    0.3010    0.7450    0.9330; % bledemodra
    0.4940    0.1840    0.5560; %fialova
    0.9290    0.6940    0.1250 % zluta
    0    0.4470    0.7410; % modra
    ];
set(groot,'defaultAxesColorOrder',colorOrder)

%% define the algos
algo{1} = 'L1(0)';
algo{2} = 'GL(0)';
algo{3} = 'eNet(0_0.2)';
algo{4} = 'denovas1(10)';
algo{5} = 'krls';
% for printing
algos{1} = '$\mathcal{R}^{L}$';
algos{2} = '$\mathcal{R}^{GL}$';
algos{3} = '$\mathcal{R}^{EN}$';
algos{4} = 'Krls';
algos{5} = 'DENOVAS';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% define 1st experiments - select 1 here
expCode = 'house'; tLVec = [100];
dataType = 'testData';
expName{1} = 'houses';
krn{1} = 'gauss5'; krn{2} = 'poly3'; krn{3} = 'lin1'; 

% get the method sparsity
for kIdx = 1:numel(krn)
  if kIdx==1
    algo{1} = 'L1(1e-05)';
    algo{2} = 'GL(1e-05)';
    algo{3} = 'eNet(1e-05_0.2)';
  else
    algo{1} = 'L1(0)';
    algo{2} = 'GL(0)';
    algo{3} = 'eNet(0_0.2)';
  end
for mIdx = 1:numel(algo)
  % load test sparsity
  load(['../nls_data/expResults/',expCode,'/testSummary_',algo{mIdx},'_',krn{kIdx},'.mat'],'sparsityTest')
  % get avg sparsityTest
  distr = mean(cell2mat(reshape(squeeze(struct2cell(sparsityTest)),1,[])),2);
  % get tentropy
  entropy{1}(kIdx,mIdx)=-sum(distr.*log(distr));
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% define 2nd experiments - select 1 here
expCode = 'Bec001'; tLVec = [40];
dataType = 'testData';
expName{2} = 'economic';


krn{1} = 'gauss7'; krn{2} = 'poly3'; krn{3} = 'lin1'; 

% get the method sparsity
for kIdx = 1:numel(krn)
  if kIdx~=3
    algo{1} = 'L1(0.0001)';
    algo{2} = 'GL(0.0001)';
  else
    algo{1} = 'L1(0)';
    algo{2} = 'GL(0)';
  end
for mIdx = 1:numel(algo)
  % load test sparsity
  load(['../nls_data/expResults/',expCode,'/testSummary_',algo{mIdx},'_',krn{kIdx},'.mat'],'sparsityTest')
  % get avg sparsityTest
  distr = mean(cell2mat(reshape(squeeze(struct2cell(sparsityTest)),1,[])),2);
  % get tentropy
  entropy{2}(kIdx,mIdx)=-sum(distr.*log(distr));
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% do the plot
f1 = figure;
set(f1,'PaperUnits','centimeters')
set(f1,'PaperPosition',[0 0 9 4])

for eIdx=1:2
  s(eIdx) = subplot(1,2,eIdx);
  bbr = bar(entropy{eIdx},'edgecolor','none','BarWidth',1);
  %hold on
  %errorbar(1:numel(algo),mse(kIdx,:),stdError(kIdx,:),'.')
  %hold off
  for mIdx = 1:numel(algo)
    set(bbr(mIdx),'facecolor',colorOrder(mIdx+1,:)); 
  end
  set(gca,'FontSize',6)
  set(gca,'XTickLabel',{'gauss','poly','lin'})
  %xlim([0 size(mse,1)+1])
  yl = ylabel('MSE','FontSize',7);
%  xl = xlabel('Dimension','FontSize',7);
  tt = title(expName{eIdx});
  set(tt,'FontSize',7);
end
ll = legend({'$\mathcal{R}^L \quad$','$\mathcal{R}^{GL} \quad$','$\mathcal{R}^{EN} \quad$','Krls','DENOVAS'},'Orientation','horizontal','Interpreter','latex');
set(ll,'FontSize',7);
set(ll,'Position',[0.05 -0.02 0.9 0.1])
set(ll,'Box','off','FontWeight','bold');

set(s(1),'Position',[0.07    0.15    0.42    0.75])
set(s(2),'Position',[0.57    0.15    0.42    0.75])

print(f1,['/home/magda/Dropbox/School/MyTexts/xNonlinearSparsity/AISTATS2018/Pics/cpDerivReal'],'-dpng');
close(f1);


end



