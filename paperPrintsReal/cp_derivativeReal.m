function cp_derivativeReal()
% CP_DERIVATIVEREAL - compare sparsity in training
%
%
% EXAMPLE: cp_sparsity()
%
% CREATED: MG - 12/10/2017
% MG Watch-out: Unlike the synthetic exp, this assumes only 1 train size 

%% nicer colors
colorOrder=[0.6350    0.0780    0.1840; % tmave cervena
    0.4660    0.6740    0.1880; % zelena
    0.8500    0.3250    0.0980; % cervena
    0.3010    0.7450    0.9330; % bledemodra
    0.4940    0.1840    0.5560; %fialova
    0    0.4470    0.7410; % modra
    0.9290    0.6940    0.1250 % zluta
    ];
set(groot,'defaultAxesColorOrder',colorOrder)

%% define the experiments - select 1 here
expCode = 'Bec001'; krn = 'gauss7'; tLVec = [40];
dataType = 'testData';

%% define the algos
algo{1} = 'L1(0)';
algo{2} = 'GL(0)';
algo{3} = 'eNet(0_0.8)';
algo{4} = 'denovas1(10)';
algo{5} = 'krls';
% for printing
algos{1} = '$\mathcal{R}^{L}$';
algos{2} = '$\mathcal{R}^{GL}$';
algos{3} = '$\mathcal{R}^{EN}$';
algos{4} = 'DENOVAS';
algos{5} = 'Krls';


%% get the method sparsity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for mIdx = 1:numel(algo)
  % load test sparsity
  load(['../nls_data/expResults/',expCode,'/testSummary_',algo{mIdx},'_',krn,'.mat'],'sparsityTest')
  % get avg sparsityTest
  sparsity(:,mIdx) = mean(cell2mat(reshape(squeeze(struct2cell(sparsityTest)),1,[])),2);
  sparsity(:,mIdx) = bsxfun(@rdivide, sparsity(:,mIdx), sum(sparsity(:,mIdx))); % and normalize to sum to 1 across all derivatives
end


f1 = figure;
set(f1,'PaperUnits','centimeters')
set(f1,'PaperPosition',[0 0 9 11])

for mIdx=1:numel(algo)
  spAll = sparsity(:,mIdx);
  %spAll = reshape(sparsity(:,mIdx),4,[])';
  s(mIdx) = subplot(1,5,mIdx);
  bbr = bar(spAll,'edgecolor','none','BarWidth',1,'FaceColo',colorOrder(6,:));
  set(gca,'FontSize',5)
  xlim([0 size(spAll,1)+1])
  if mIdx==1
    yl = ylabel('$|| \partial_a f ||_{2_n}$','Interpreter','latex','FontSize',9);
  end
  xl = xlabel('Dimension','FontSize',7);
  tt = title(algos{mIdx},'Interpreter','latex');
  set(tt,'FontSize',7);
end
%set(ll,'FontSize',7);
%set(ll,'Position',[0.15 -0.02 0.8 0.1])
%set(ll,'Box','off','FontWeight','bold');
%set(ll,'FontSize',20);

% set(s(1),'Position',[0.09    0.73    0.42    0.23])
% set(s(2),'Position',[0.57    0.73    0.42    0.23])
% set(s(3),'Position',[0.09    0.43    0.42    0.23])
% set(s(4),'Position',[0.57    0.43    0.42    0.23])
% set(s(5),'Position',[0.09    0.12    0.42    0.23])
% set(s(6),'Position',[0.57    0.12    0.42    0.23])

print(f1,['/home/magda/Dropbox/School/MyTexts/xNonlinearSparsity/AISTATS2018/Pics/cpDerivReal_',expCode,krn],'-dpng');
close(f1);


end



