%% MG 10/10/2017 - understand the learned model

clear
expCode = 'house';
algo = 'L1(10)';
kernelType = 'poly';
kernelParam = 3;

%% check validation summary for best MSE
[sel,spTrain,spVal,minPIdx,pGrid] = checkLearning(expCode,algo,kernelType,kernelParam);

minPIdx
pGrid
bar(mean(spTrain,2))


% get kernel matrices
load(['../nls_data/expResults/',expCode,'/model_',algo,'_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'])
load(['../nls_data/expResults/',expCode,'/test_',algo,'_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'])


load(['../nls_data/expResults/',expCode,'/valid_',algo,'_',num2str(trainLength),'_',num2str(rep),'_',kernelType,num2str(kernelParam),'.mat'])
[K,Da,~,Lab,y] = getKernels(expCode,'train',trainLength,rep,kernelType,kernelParam);
% need these to get the predictions
n = size(K,2); d = size(Da,3);
MaTemp = permute(Da,[1,3,2]); D = reshape(MaTemp,[],n);
MaTemp = permute(Lab,[1,2,4,3]); La = reshape(MaTemp,n,[],d);
MaTemp = permute(Lab,[1,3,2,4]); Lb = reshape(MaTemp,[],n,d);
MaTemp = permute(La,[1,3,2]); L = reshape(MaTemp,[],n*d);

J3 = a{minParamIdx.tL_100}'*K*a{minParamIdx.tL_100}+...
b{minParamIdx.tL_100}'*L*b{minParamIdx.tL_100}+...
a{minParamIdx.tL_100}'*D'*b{minParamIdx.tL_100}

[D(1:10,:)*a{minParamIdx.tL_100} L(1:10,:)*b{minParamIdx.tL_100}]

[selected{:}]
sparsityTrain
sparsityVal


%The function values
funcVal = K*a{minParamIdx.tL_100}+D'*b{minParamIdx.tL_100}
% 
% 
% 
