function MG_testKernels
%%%%%% MG 23/9/2017 test kernel construction %%%%%%
%Kernels seem ok%

x=rand(5,3);

%[K,Da,Da1,Lab] = getKernelMatrices(x,x,'gauss',4);

%[K,Da,Da1,Lab] = getKernelMatrices(x,x,'poly',4);

[K,Da,Da1,Lab] = getKernelMatrices(x,x,'lin',1);

KDen = k_lin(x);
ZDen = z_lin(x);
LDen = l_lin(x);

Da-Da1

K-KDen

[Da(:,:,1);Da(:,:,2);Da(:,:,3)]' - ZDen

[Lab(:,:,1,1) Lab(:,:,1,2) Lab(:,:,1,3); ...
Lab(:,:,2,1) Lab(:,:,2,2) Lab(:,:,2,3); ...
Lab(:,:,3,1) Lab(:,:,3,2) Lab(:,:,3,3)] - LDen





function K = k_gauss(X,sigma)
n = size(X,1);
K = zeros(n);
var=(2*(sigma^2));
for i = 1:n; 
    for j=1:n;
        K(i,j) = exp(-norm(X(i,:)-X(j,:))^2/var);
    end; 
end;

function Z = z_gauss(X,sigma)
[n,d] = size(X);
Z = zeros(n,n*d);
for i = 1:n; 
    for a=1:d; 
        for j=1:n;
            Z(i,(a-1)*n+j) = 1/sigma^2*(X(i,a)-X(j,a))*exp(-1/(2*sigma^2)*norm(X(i,:)-X(j,:))^2);
        end; 
    end; 
end;

function L = l_gauss(X,sigma)
[n,d] = size(X);
dist = zeros(n,n);
for i = 1:n;
    for j =1:n;
        dist(i,j) = exp(-1/(2*sigma^2)*norm(X(i,:)-X(j,:))^2)./(sigma^4);
    end
end
X1 = repmat(cell2mat(mat2cell(repmat(reshape(X,n*d,1),1,n),n*ones(d,1),n)'),d,1);
X2 = repmat(reshape(X,n*d,1),1,n*d);
L = -(X2-X1').*(X1-X2').*repmat(dist,d,d);
for a=1:d;
    L(((a-1)*n+1):(a*n),((a-1)*n+1):(a*n)) = L(((a-1)*n+1):(a*n),((a-1)*n+1):(a*n)) + (sigma^2).*dist;
end


function K = k_pol(X,p)
K = (X*X'+1).^p;

function Z = z_pol(X,p)

[n,d] = size(X);
Z = zeros(n,n*d);
for i = 1:n; 
    for a=1:d; 
        for j=1:n; 
            Z(i,(a-1)*n+j) = p*(sum(X(i,:).*X(j,:))+1)^(p-1)*X(i,a); 
        end; 
    end; 
end

function L = l_pol(X,p)


[n,d] = size(X);
XX = repmat(cell2mat(mat2cell(repmat(reshape(X,n*d,1),1,n),n*ones(d,1),n)'),d,1);
L = repmat(((p-1)*p).*(X*X'+1).^(p-2),d,d).*XX.*XX';
for a=1:d;
    L(((a-1)*n+1):(a*n),((a-1)*n+1):(a*n)) = L(((a-1)*n+1):(a*n),((a-1)*n+1):(a*n)) + p.*(X*X'+1).^(p-1);
end


function K = k_lin(X)
K = X*X';

function Z = z_lin(X)
[n,d] = size(X);
Z = zeros(n,n*d);
for i = 1:n; 
    for a=1:d; 
        for j=1:n; 
            Z(i,(a-1)*n+j) = X(i,a); 
        end; 
    end; 
end

function L = l_lin(X)
[n,d] = size(X);
L = zeros(n*d,n*d);
for i = 1:n; 
    for a=1:d; 
        for j=1:n;
            for b = 1:d;
                if a==b;
                    L((a-1)*n+i,(b-1)*n+j) = 1;
                end
            end
        end; 
    end; 
end; 


