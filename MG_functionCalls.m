function MG_functionCalls
%%% MG 25/4/2017 fucntion calls


compareModels('eco002','gauss4')
compareModels('Bec001','lin1')


clear
load('/home/magda/Bitbucket/nls_data/expResults/house/model_krls_50_1_gauss4')
[a{:}]

clear
validateModels('house',50,1,'eNet','gauss',4,0.01,0.5)



trainModels('ShuffHouse',50,5,'L1','poly',3,-6,0,10,0)

trainModels('ShuffHouse',50,1,'GL','lin',1,-3,0,10,0,[1 1 1 1 1 1 2 2 2 2 2 2])

trainModels('house',50,5,'eNet','gauss',4,-3,0,10,0.5,0.001)

trainModels('Bec001',40,2,'GL','gauss',7,-2,1,20,0,[1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4 5 5 5 5 6 6 6 6 7 7 7 7])


%trainModels(expCode,trainLength,rep,algo,kernelType,kernelParam,varargin)
trainModels('eco001',100,1,'L1','polyHom',3,-3,0,30)
trainModels('eco001',90,1,'L1','gauss',4,-5,0,30)
trainModels('house',300,5,'L1H','gauss',4,-3,0,30,10)

trainModels('exp005',150,1,'L1','gauss',4,-3,0,30)



%% median of pair-wise distances
clear
load('../nls_data/expData/Eec002/trainData40_1.mat')
xTr = x;
% % load('../nls_data/expData/eco001/testData.mat')
% xTr = [xTr;x];
% % load('../nls_data/expData/eco001/validData.mat')
% xTr = [xTr;x];
xTe = xTr;
% get the pair-wise distances

% %Version1:
% tic
% n = size(xTr,1);
% n1 = size(xTe,1);
% for i = 1:n
%   for j = 1:n1
%     k(i,j) = norm(xTr(i,:)-xTe(j,:))^2;
%   end
% end
% toc

%Version2:
tic
rowsqTr = sum(xTr.^2,2);
rowsqTe = sum(xTe.^2,2);
prodX = 2*xTr*xTe';
KTemp = bsxfun(@minus,rowsqTr,prodX);
KTemp = bsxfun(@plus,KTemp,rowsqTe');
toc
%norm(k-KTemp)

U = triu(KTemp);
U = (U(find(U>0)));
U = U.^(1/2);

[mean(U) median(U)]
quantile(U,[0.1 0.25 0.5 0.75 0.9])


% Statistics for Bec size 160
% Mean Std
% Train1: 6.8711    6.6095
% Train10: 7.2778    6.7920
% Train30: 7.0277    6.6795


% Statistics for eco size 160
% Mean Std
% Train1: 4.5849    4.2401
% Train10: 4.7790    4.3108
% Train30: 4.4887    4.0095


% Statistics for hosue
% Mean Std
% Train1: 5.5239       5.1329
% Train5: 5.6875       5.2663
% Train+Test+Valid 5.6664       5.2526

% Statistics for exp001
% Mean Std
% Train1: 13.1507    3.6052
% Train10: 13.1237    3.5713
% Train15: 13.5703    3.5678
% Test: 13.2932    3.5089
% Valid: 13.3971    3.5243

% Statistics for exp006 150size
% Mean Std
% Train1: 7.9951    2.6580
% Train3: 7.8499    2.7416
% Train5: 8.1425    2.7220
% Test: 8.0163    2.7389
% Valid: 7.9502    2.7218

% Statistics for hosue
% Mean Std
% Train1: 44.3261  102.2330
% Test: 41.9567  100.2465
% Valid: 43.3794   88.8735




end
